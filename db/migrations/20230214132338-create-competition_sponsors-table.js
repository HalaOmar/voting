'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('competition_sponsors', {
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
      },
      competition_id: {
        primaryKey: true,
        type: Sequelize.STRING,
        references: {
          model: 'competitions',
          key: 'competition_id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      sponsor_id: {
        primaryKey: true,
        type: Sequelize.STRING,
        references: {
          model: 'sponsors',
          key: 'sponsor_id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('competition_celebrities');
  }
};