'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_purchases_tools', {
      competition_id: {
        type: Sequelize.STRING,
        references: {
          model: 'competitions',
          key: 'competition_id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      tool_id: {
          type: Sequelize.STRING,
          references: {
            model: 'tools',
            key: 'tool_id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
      },
      user_tools_line_id : {
          type : Sequelize.STRING ,
          references : {
              model : 'user_purchases' , 
              key   : 'user_tools_line_id'        
          } ,
          onDelete  : 'cascade' ,
          onUpdate  : 'cascade'
      } ,
      quentity : {
          type : Sequelize.INTEGER , 
          allowNull :false
      } ,
      price    :{
          type : Sequelize.INTEGER(8,10)
      } ,
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_purchases_tools');
  }
};