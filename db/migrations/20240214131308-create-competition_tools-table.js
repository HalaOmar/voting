'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('competition_tools', {
      competition_id: {
        type: Sequelize.STRING,
        references: {
          model: 'competitions',
          key: 'competition_id',
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      tool_id: {
        primaryKey: true,
        type: Sequelize.STRING,
        references: {
          model: 'tools',
          key: 'tool_id' ,
          onUpdate: 'cascade',
          onDelete: 'cascade'

        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('competition_tools');
  }
};