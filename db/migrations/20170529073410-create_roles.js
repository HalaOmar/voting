'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('roles', {
      role_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      role_name_ar: {
        type: Sequelize.STRING
      },
      role_name_en: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('roles');
  }
};
