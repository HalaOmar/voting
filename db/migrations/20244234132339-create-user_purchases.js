'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_purchases', {
      
  user_tools_line_id : {
      type : Sequelize.STRING ,
      primaryKey : true
  } ,
  user_id : { 
      type: Sequelize.STRING,
      references: {
        model: 'users',
        key: 'user_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
  },
  total_points : {
      type : Sequelize.INTEGER,
      defaultValue : 0
  },
  total_price : {
    type : Sequelize.INTEGER ,
    defaultValue : 0
  },
  created_at: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },
  updated_at: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_purchases');
  }
};