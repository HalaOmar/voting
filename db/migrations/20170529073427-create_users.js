'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      user_id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      full_name: {
        type: Sequelize.STRING,
      },
      mobile: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      email: {
        type: Sequelize.STRING
      },
      avatar_image: Sequelize.STRING,
      s3_id: Sequelize.STRING,
      blocked: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      date_of_birth: {
        type: Sequelize.DATEONLY,
        allowNull: true,
      },
      gender: Sequelize.INTEGER,
      role_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
        references: {
          model: 'roles',
          key: 'role_id'
        },
        onUpdate: 'cascade',
        onDelete: 'restrict',
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    }).then(() => queryInterface.addIndex('users', ['full_name']));
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};
