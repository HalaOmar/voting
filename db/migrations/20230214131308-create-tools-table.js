'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tools', {
      tool_id: {
        primaryKey: true,
        type: Sequelize.STRING,
      },
      tool_name : { 
        type : Sequelize.STRING
      },
      points:{ 
        allowNull: false,
        type :Sequelize.INTEGER,
        defaultValue: 0
      },
      price: {
        allowNull: false,
        type :Sequelize.INTEGER,
        defaultValue: 0
  
      },
      s3_id: {
        allowNull: false,
        type :Sequelize.STRING
      },
      competition_id : {
        type : Sequelize.STRING ,
        references : {
            model : 'competitions' ,
            key   :'competition_id'
        } ,
        onDelete  :'cascade',
        onUpdate  :'cascade'
    },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tools');
  }
};