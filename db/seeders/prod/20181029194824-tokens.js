'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('access_tokens',
            [
                {
                    user_id: 0,
                    access_token: '5657bbf137ee875af28767f088e09bf16ef9229c1678c11fea0b49fea2ab3f76',
                    expire_at: '2025-01-14 18:52:09',
                    created_at: new Date(),
                    updated_at: new Date()
                }
            ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('access_tokens', null, {});
    }
};
