'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users',
      [
        {
          user_id: 0,
          full_name: 'Ahmed Isam',
          mobile: '+966547771884',
          role_id: 4,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          user_id: 2,
          full_name: 'Hala Omar',
          mobile: '+966568227578',
          role_id: 4,
          created_at: new Date(),
          updated_at: new Date()
        }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
