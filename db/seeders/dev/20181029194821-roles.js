'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('roles',
      [
        {
          role_id: 1,
          role_name_en: 'USER',
          role_name_ar: 'مستخدم',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          role_id: 2,
          role_name_en: 'Celebrity',
          role_name_ar: 'مشهور',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          role_id: 3,
          role_name_en: 'SUPERVISOR',
          role_name_ar: 'مشرف',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          role_id: 4,
          role_name_en: 'Admin',
          role_name_ar: 'مدير نظام',
          created_at: new Date(),
          updated_at: new Date()
        }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('roles', null, {});
  }
};
