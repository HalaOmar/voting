'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('celebrities',
      [
        {
          celebrity_id: "12345",
          full_name: 'Hala Omar',
          gender: 2,
          mobile: '+966568227578',
          social_links: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          celebrity_id: "67890",
          full_name: 'Reema Moaz',
          gender: 2,
          mobile: '+966563425757',
          social_links: null,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          celebrity_id: "37767",
          full_name: 'Omar Omar',
          gender: 1,
          mobile: '+966561234578',
          social_links: "http://omardomain:myprofile",
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          celebrity_id: "90164",
          full_name: 'Mohammad Khaled',
          gender: 1,
          mobile: '+966468225261',
          social_links: null,
          created_at: new Date(),
          updated_at: new Date()
        },        {
          celebrity_id: "67901",
          full_name: 'Raneem Ali',
          gender: 2,
          mobile: '+966482993670',
          social_links: null,
          created_at: new Date(),
          updated_at: new Date()
        },

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
