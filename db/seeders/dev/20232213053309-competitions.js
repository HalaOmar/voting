'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('competitions',
      [
        {
          competition_id: "10",
          started_at:new Date(),
          ended_at: new Date(),
          active: 1,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          competition_id: "15",
          started_at:new Date(),
          ended_at: new Date(),
          active: 0,
          created_at: new Date(),
          updated_at: new Date()
        }

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
