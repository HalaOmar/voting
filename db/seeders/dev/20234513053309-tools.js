'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('tools',
      [
        {
          tool_id : "61",
          tool_name : "spider",
          points :40 ,
          price :30, 
          s3_id : "spider", 
          competition_id :10 ,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          tool_id : "2",
          tool_name : "T-rex",
          points :80 ,
          price :40, 
          s3_id : "treax-image", 
          competition_id :10 ,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          tool_id : "3",
          tool_name : "scorpion",
          points : 100 ,
          price : 150, 
          s3_id : "scorpion", 
          competition_id :15 ,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          tool_id : "4",
          tool_name : "dinasour",
          points : 150 ,
          price : 200, 
          s3_id : "spider", 
          competition_id :15 ,
          created_at: new Date(),
          updated_at: new Date()
        },

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
