const competitionVotingService = require('../competition_voting_service')
const resWraper = require('../../commons/http_res_wrapper')


exports.getVotingHistory = async ( req , res ) =>{

    try{

    let paginationObj ={
        page_number : req.query.page_number ? parseInt(req.query.page_number):0 ,
        page_size   : req.query.page_size ? parseInt(req.query.page_size):5
    }

    console.log(paginationObj);

    let history = await competitionVotingService.getVotingHistory(paginationObj)
   
    resWraper.success( res , history)

}catch(e){

    resWraper.error( res , e)
}

}