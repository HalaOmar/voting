const express = require('express')
const router  = express.Router()
const competitionVotingController = require('./competition_voting_controller')
const auth_controller  = require('../../auth/web_app/auth_controller')

router.route('/')
.get(auth_controller.authenticate , 
    auth_controller.isAdminOrSupervisor ,
    competitionVotingController.getVotingHistory)

module.exports = router