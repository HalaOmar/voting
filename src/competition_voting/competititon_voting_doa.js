const models = require('../models/index')
const CompetitionVoting = models.competition_votings
const sequelize         = require('sequelize')
const fullInclude = [ { model : models.celebrities , as : 'celebrity'}, 
                      { model: models.competitions , as : 'competition' },
                      { model: models.users , as : 'user' },

                    ]


exports.voteToCelebrity =  ( vote ) => {

  return  CompetitionVoting.create(
        {
            user_id : vote.userId ,
            competition_id : vote.competitionId , 
            celebrity_id : vote.celebrityId
        }
    )

}

exports.getTheWinner = async ( ) => {

  return  CompetitionVoting.count({
      group:['celebrity_id']  ,
  })

}


exports.getTop5 = () =>  {
  
  return  CompetitionVoting.findAll({
	
  attributes: ['*', [sequelize.fn('count', sequelize.col('competition_votings.celebrity_id')), 'votes']],

  group : ['celebrity_id'],

  order: sequelize.literal('votes DESC'),

  include : fullInclude

});
}

exports.getVotingHistory = ( page_number , page_size) =>{

  return CompetitionVoting.findAll({

    include: fullInclude,

    offset : page_number ,

    limit  : page_size
  })

    
}