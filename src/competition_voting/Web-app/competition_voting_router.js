const express = require('express')
const router  = express.Router()
const authController = require('../../auth/web_app/auth_controller')
const competitionVotingController = require('./competition_voting_controller')


router.route('/').
post( authController.authenticate , 
    competitionVotingController.voteToMyCelebrity).
get(competitionVotingController.theWinner)

router.route('/top5')
.get(authController.authenticate ,
    competitionVotingController.getTopFive )



module.exports = router


