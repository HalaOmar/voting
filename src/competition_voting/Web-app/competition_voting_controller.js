
const resWraper = require('../../commons/http_res_wrapper')
const competitionVotingService = require('../competition_voting_service')
const CompetitionVoting = require('../competititon_voting_doa')

exports.voteToMyCelebrity = async ( req , res ) =>{

    try{
        
        let vote = {
            userId   :  req.body.user_id ? req.body.user_id:null ,
            celebrityId : req.body.celebrity_id ? req.body.celebrity_id :null,
            competitionId : req.body.competition_id ? req.body.competition_id :null
        }
 
    let status  = await competitionVotingService.voteToCelebrityDiscountUserPoints(vote)
    let topFive = await CompetitionVoting.getTop5()
    topFive     = topFive.map(celebrity => celebrity.toJSON())
    res.locals.io.emit('update_votes_num' , topFive)
    
        resWraper.success(res , status)
    }catch ( e) {

        resWraper.error(res , e )

    }
}

exports.theWinner = async ( req , res ) =>{

    try{
        let winner = await competitionVotingService.getTheWinner()
        resWraper.success( res ,winner)

    }catch(e){
        resWraper.error(res , e )

    }

 res.send (  )
}

exports.getTopFive = async ( req , res ) =>{
try{
    let result =  await competitionVotingService.getTopFive()
    resWraper.success(res , result)

}catch(e){
    resWraper.error( res , e )

}
    
   }