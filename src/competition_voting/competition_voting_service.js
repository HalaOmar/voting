const competitionVotingDao  = require ('./competititon_voting_doa')
const userPonintsServise        = require ('../user_points/user_points_service')
const celebrityDao   = require('../celebrity/celebrity_dao')
const Error = require ( '../commons/errors/exceptions')
const i18n = require('i18n')

exports.voteToCelebrityDiscountUserPoints = async ( vote ) =>{

   let userbalance = await userPonintsServise.userVoteToCelebrity(vote)
    if(!userbalance) {
        throw new Error.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
    }
   let celebrityVoted = await competitionVotingDao.voteToCelebrity(vote)
    
    return Promise.resolve(celebrityVoted)


}

exports.getTheWinner = async ( ) => {

   let result = await competitionVotingDao.getTheWinner()
   

   let new_result = result.sort(function(a, b){return b.count-a.count});

   console.log(`new_result \n \n `); 

   console.log(new_result); 

   let winner     = await celebrityDao.getById(new_result[0].celebrity_id)

                                             
    return Promise.resolve(winner)
}

exports.getVotingHistory = async (  paginationObj ) =>{

    let { page_number , page_size } = paginationObj

    if( page_number < 0 || page_size <= 0){
        throw new Error.InvalidInputException("Invalid Input Exception")
    }
    
    let results = await competitionVotingDao.getVotingHistory(page_number , page_size)

    results = results.map( record =>  record.toJSON())

    let filterd_history = historyAbstract(results)

    return Promise.resolve(filterd_history)

}
exports.getTopFive = async  ( ) => {
    
    let topFive = []

    let result = await competitionVotingDao.getTop5()

    result_json     = result.map( record => {return record.toJSON()})

    topFive         = resultFilter(result_json).slice(0,6)

    return Promise.resolve(topFive)
       
 }

function resultFilter(arr ){

    let voted_celebrities = []

    arr.forEach(element => {

        let voted_celebrity = { }

        let { votes , celebrity } = element

        voted_celebrity.votes        = votes
        voted_celebrity.full_name    = celebrity.full_name
        voted_celebrity.s3_id        = celebrity.s3_id
        voted_celebrity.celebrity_id = celebrity.celebrity_id

        voted_celebrities.push(voted_celebrity)

    });

    return voted_celebrities

} 	

function historyAbstract(detailed_history) {

    let result = detailed_history.map( record => { 

        let { celebrity , user , created_at  } = record

        let newObj = { 
            celebrity_name  : celebrity.full_name , 
            celebrity_phone : celebrity.mobile , 
            user_name       : user.full_name , 
            user_phone      : user.mobile ,
            vote_date       : created_at
        }
        return newObj   
    })

    return result
   
}
