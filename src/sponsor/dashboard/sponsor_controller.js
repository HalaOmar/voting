const resWrapper = require('../../commons/http_res_wrapper')
const sponsorService = require('../sponsor_service')

exports.addSponsor = async (req, res) => {
  let sponsor = {
    name: req.body.name ? req.body.name : null,
    mobile: req.body.mobile ? req.body.mobile : null,
    file: req.files ? req.files[0].path : null,
    url_link: req.body.url_link ? req.body.url_link : null,
  };

  try {
    let result = await sponsorService.addSponsor(sponsor);
    resWrapper.success(res, result);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.updateSponsor = async (req, res) => {
  let sponsorId = req.params.sponsor_id ? req.params.sponsor_id : null;
  let sponsor = {
    sponsorId: sponsorId,
    name: req.body.name ? req.body.name : null,
    mobile: req.body.mobile ? req.body.mobile : null,
    url_link: req.body.url_link ? req.body.url_link : null,
    file: req.files.length ? req.files[0].path : null,
  }

  try {
    let updateResult = await sponsorService.updateSponsor(sponsorId, sponsor);
    resWrapper.success(res, updateResult);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.deleteSponsor = async (req, res) => {
  let sponsorId = req.params.sponsor_id;

  try {
    await sponsorService.deleteSponsor(sponsorId);
    resWrapper.success(res);
  } catch (e) {
    resWrapper.error(res, e);
  }
}

exports.getSponsorById = async (req, res) => {
  let sponsorId = req.params.sponsor_id;

  try {
    let result = await sponsorService.getSponsorById(sponsorId);
    resWrapper.success(res, result);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getAllSponsors = async (req, res) => {
  try {
    let results = await sponsorService.getAllSponsors();
    resWrapper.success(res, results);
  } catch (e) {
    resWrapper.error(res, e);
  }
};