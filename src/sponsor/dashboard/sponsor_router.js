const express = require('express');
const router = express.Router()
const authController = require('../../auth/web_app/auth_controller');
const uploadController = require('../../commons/upload_middleware');
const sponsorController = require('./sponsor_controller')

router.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    sponsorController.getAllSponsors)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    sponsorController.addSponsor);

router.route('/:sponsor_id')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    sponsorController.getSponsorById)
  .put(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    sponsorController.updateSponsor)
  .delete(authController.authenticate,
    authController.isAdminOrSupervisor,
    sponsorController.deleteSponsor);

module.exports = router