const models = require('../models/index');
const Sponsor = models.sponsors;

exports.addSponsor = async (sponsor) => {
  let dto = {
    sponsor_id: sponsor.sponsor_id,
    name: sponsor.name,
    url_link: sponsor.url_link,
    mobile: sponsor.mobile,
    s3_id: sponsor.s3_id,
  };
  let result = await Sponsor.create(dto);
  return Promise.resolve(result.toJSON());
}

exports.updateSponsor = async (sponsorId, sponsor) => {
  await Sponsor.update(sponsor, {
    where: {
      sponsor_id: sponsorId,
    }
  });
};

exports.deleteSponsor = async (sponsorId) => {
  await Sponsor.destroy({
    where: {
      sponsor_id: sponsorId
    }
  });

  return Promise.resolve();
};

exports.getById = async (sponsorId) => {
  let result = await Sponsor.findOne({
    where: {
      sponsor_id: sponsorId
    }
  });

  if (result) {
    result = result.toJSON();
  }

  return Promise.resolve(result);
};

exports.getAllSponsors = async () => {
  let results = await Sponsor.findAll();
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
};