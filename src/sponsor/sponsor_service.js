const i18n = require('i18n');
const sponsorDao = require('./sponsor_doa')
const idGenerator = require('../commons/id_generator');
const Errors = require('../commons/errors/exceptions');
const MessageKeys = require('../commons/constants').MessageKeys;
const s3Uploader = require('../commons/s3Uploader');

exports.addSponsor = async (sponsorDto) => {
  let sponsor = {
    sponsor_id: idGenerator.generateId(),
    name: sponsorDto.name,
    url_link: sponsorDto.url_link,
    mobile: sponsorDto.mobile,
    s3_id: null,
  };

  if (!sponsor.mobile && !sponsor.name)
    throw new Errors.InvalidInputException();

  if (sponsorDto.file) {
    let result = await s3Uploader.uploadAvatar(sponsorDto.file);
    sponsor.s3_id = result.s3_id ? result.s3_id : null;
  }

  return sponsorDao.addSponsor(sponsor);
}

exports.updateSponsor = async (sponsorId, sponsorDto) => {
  if (!sponsorId || !sponsorDto.mobile || !sponsorDto.name)
    throw new Errors.InvalidInputException();

  let existSponsor = await sponsorDao.getById(sponsorId);
  if (!existSponsor) {
    throw new Errors.NotFoundException();
  }

  let sponsor = {
    sponsor_id: sponsorId,
    name: sponsorDto.name,
    url_link: sponsorDto.url_link,
    mobile: sponsorDto.mobile
  }

  if (sponsorDto.file) {
    let result = await s3Uploader.uploadAvatar(sponsorDto.file);
    sponsor.s3_id = result.s3_id;
  }

  await sponsorDao.updateSponsor(sponsorId, sponsor);
  return sponsorDao.getById(sponsorId);
}

exports.deleteSponsor = async (sponsorId) => {
  if (!sponsorId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  let sponsor = await sponsorDao.getById(sponsorId);
  if (!sponsor) {
    throw new Errors.NotFoundException();
  }

  await sponsorDao.deleteSponsor(sponsorId);
  return Promise.resolve();
}

exports.getSponsorById = async (sponsorId) => {
  if (!sponsorId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  let sponsor = await sponsorDao.getById(sponsorId);
  if (!sponsor) {
    throw new Errors.NotFoundException();
  }

  return Promise.resolve(sponsor);
}

exports.getAllSponsors = () => {
  return sponsorDao.getAllSponsors();
}

