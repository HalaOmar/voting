const i18n = require('i18n');
const Errors = require('../commons/errors/exceptions');
const celebrityDao = require('./celebrity_dao');
const s3Uploader = require('../commons/s3Uploader');
const logger = require('../libs/logger');
const {MessageKeys} = require('../commons/constants');
const id_generator = require('../commons/id_generator');

exports.registerCelebrity = async (celebrityDto) => {
  let celebrity = {
    celebrity_id: id_generator.generateId(),
    full_name: celebrityDto.full_name,
    gender: celebrityDto.gender,
    s3_id: null,
    mobile: celebrityDto.mobile,
    social_links: celebrityDto.social_links,
  };

  // TODO: Implement validation
  if (!celebrity.full_name) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  if (celebrity.file) {
    let result = await s3Uploader.uploadAvatar(celebrity.file);
    celebrity.s3_id = result.s3_id;
  }

  let createdCelebrity = await celebrityDao.createNewCelebrity(celebrity);
  return Promise.resolve(createdCelebrity);
};

exports.updateCelebrity = async (celebrityId, celebrityDto) => {
  let celebrity = await celebrityDao.getById(celebrityId);
  if (!celebrity) {
    throw new Errors.UserNotFoundException();
  }

  celebrity = {
    full_name: celebrityDto.full_name,
    gender: celebrityDto.gender,
    mobile: celebrityDto.mobile,
    social_links: celebrityDto.social_links,
  };

  if (celebrity.file) {
    let result = await s3Uploader.uploadAvatar(celebrity.file);
    celebrity.s3_id = result.s3_id;
  }

  await celebrityDao.updateCelebrity(celebrityId, celebrity);
  let updatedCelebrity = await celebrityDao.getById(celebrityId);

  return Promise.resolve(updatedCelebrity);
};

exports.getAllCelebrities = async (pageSize = 20, pageNumber = 0, mobile, gender,
                                   searchKeyWord, withTotalCount) => {
  return celebrityDao.getAllCelebrities(pageSize, pageNumber, mobile, gender, searchKeyWord, withTotalCount);
};

exports.getCelebritiesCount = async () => {
  return celebrityDao.getCelebritiesCount();
};

exports.getById = async (celebrityId) => {
  let celebrity = await celebrityDao.getById(celebrityId);
  if (celebrity === null)
    throw new Errors.UserNotFoundException();

  return Promise.resolve(celebrity);
};