const models = require('../models/index');
const Celebrity = models.celebrities;
const Op = models.Sequelize.Op;

exports.getAll = async () => {
  let celebrities = await Celebrity.findAll();
  celebrities = celebrities.map(celebrity => celebrity.toJSON());
  return Promise.resolve(celebrities);
};

exports.getAllCelebrities = async (pageSize = 10, pageNumber = 0, mobile, gender,
                                   searchKeyWord, withTotalCount = false) => {
  let offset = pageNumber * pageSize;
  let order = [["created_at", "DESC"]];
  let options = {
    order: order,
    offset: offset,
    limit: pageSize,
    raw: false,
  };

  options.where = {};

  if (searchKeyWord) {
    let search = {
      [Op.or]: [
        {full_name: {[Op.substring]: searchKeyWord}},
        {mobile: {[Op.substring]: searchKeyWord}}
      ]
    };
    options.where = search;
  }

  if (mobile) {
    options.where.mobile = {
      [Op.substring]: mobile
    }
  }

  if (gender) {
    options.where.gender = gender
  }

  if (!withTotalCount)
    return _getCelebrities(options);
  else
    return _getCelebritiesWithCount(options, pageNumber, pageSize);
};

exports.getCelebritiesCount = async () => {
  let totalCelebrities = await Celebrity.count();
  return {total: totalCelebrities};
};

exports.getById = async (celebrityId) => {
  let celebrity = await Celebrity.findOne({
    where: {
      celebrity_id: celebrityId
    },
  });

  if (celebrity) {
    celebrity = celebrity.toJSON();
  }

  return Promise.resolve(celebrity);
};

exports.getByMobile = async (mobile) => {
  let result = await Celebrity.findOne({
    where: {
      mobile: mobile
    },
  });

  if (result) {
    result = result.toJSON()
  }

  return Promise.resolve(result);
};

exports.createNewCelebrity = async (celebrity) => {
  let dto = {
    celebrity_id: celebrity.celebrity_id,
    full_name: celebrity.full_name,
    gender: celebrity.gender,
    s3_id: celebrity.s3_id,
    mobile: celebrity.mobile,
    social_links: JSON.stringify(celebrity.social_links),
  };

  let result = await Celebrity.create(dto);
  return Promise.resolve(result.toJSON())
};

exports.updateCelebrity = (celebrityId, celebrity) => {
  return Celebrity.update(celebrity, {
    where: {
      celebrity_id: celebrityId
    }
  })
};

exports.mapCelebrities = (result) => {
  return result.map(celebrity => {
    return this.mapCelebrity(celebrity);
  });
};

exports.mapCelebrity = (user) => {
  let res = user.toJSON();

  if (res.role)
    res.role = res.role.toJSON();

  return res;
};

_getCelebrities = async (options) => {
  let celebrities = await Celebrity.findAll(options);
  celebrities = this.mapCelebrities(celebrities);
  return Promise.resolve(celebrities);
};

_getCelebritiesWithCount = async (options, pageNumber, pageSize) => {
  let results = await Celebrity.findAndCountAll(options);
  results.rows = this.mapCelebrities(results.rows);

  let response = {
    results: results.rows,
    page_number: pageNumber,
    page_size: pageSize,
    results_count: results.rows.length,
    total_count: results.count,
  };

  return Promise.resolve(response);
};