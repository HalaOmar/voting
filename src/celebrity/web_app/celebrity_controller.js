const i18n = require('i18n');
const celebrityService = require('../celebrity_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.registerCelebrity = async (req, res) => {
  let celebrity = {
    full_name: req.body.full_name,
    mobile: req.body.mobile,
    gender: req.body.gender,
    social_links: req.body.social_links,
  };

  if (req.files.length > 0) {
    celebrity.avatar_image = req.files[0].filename;
    celebrity.file = req.files[0].path
  }

  try {
    let updatedUser = await celebrityService.registerCelebrity(celebrity);
    resWrapper.success(res, updatedUser);
  } catch (e) {
    resWrapper.error(res, e);
  }
};