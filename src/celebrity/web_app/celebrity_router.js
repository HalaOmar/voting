const express = require('express');
const router = express.Router();
const celebrityController = require('./celebrity_controller');
const uploadController = require('../../commons/upload_middleware');

router.route('/')
  .post(uploadController.uploadFiles,
    celebrityController.registerCelebrity);

module.exports = router;
