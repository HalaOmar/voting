const i18n = require('i18n');
const celebrityService = require('../celebrity_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.getById = async (req, res) => {
  let celebrityId = req.params.celebrity_id;
  try {
    let celebrity = await celebrityService.getById(celebrityId);
    resWrapper.success(res, celebrity);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.listCelebrities = async (req, res) => {
  let pageNumber = req.query.page_number ? parseInt(req.query.page_number) : 0;
  let pageSize = (req.query.page_size && req.query.page_size < 50) ? parseInt(req.query.page_size) : 20;
  let mobile = req.query.mobile ? req.query.mobile : null;
  let gender = req.query.gender ? req.query.gender : null;
  let searchKeyWord = req.query.search;
  let withTotalCount = true;

  try {
    let celebrities = await celebrityService.getAllCelebrities(pageSize, pageNumber, mobile, gender,
      searchKeyWord, withTotalCount);
    resWrapper.success(res, celebrities);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getCelebritiesCount = async (req, res) => {
  try {
    let celebrities = await celebrityService.getCelebritiesCount();
    resWrapper.success(res, celebrities);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.updateCelebrity = async (req, res) => {
  let celebrityId = req.params.celebrity_id;

  let dto = {
    full_name: req.body.full_name,
    mobile: req.body.mobile,
    gender: req.body.gender,
    social_links: (req.body.social_links === null || req.body.social_links === undefined) ? null : req.body.social_links
  };

  if (req.files.length > 0) {
    dto.avatar_image = req.files[0].filename;
    dto.file = req.files[0].path
  }

  try {
    let updatedCelebrity = await celebrityService.updateCelebrity(celebrityId, dto);
    resWrapper.success(res, updatedCelebrity);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.registerCelebrity = async (req, res) => {
  let celebrity = {
    full_name: req.body.full_name,
    mobile: req.body.mobile,
    gender: req.body.gender,
    social_links: (req.body.social_links === null || req.body.social_links === undefined) ? null : req.body.social_links
  };

  if (req.files.length > 0) {
    celebrity.avatar_image = req.files[0].filename;
    celebrity.file = req.files[0].path
  }

  try {
    let createdCelebrity = await celebrityService.registerCelebrity(celebrity);
    resWrapper.success(res, createdCelebrity);
  } catch (e) {
    resWrapper.error(res, e);
  }
};


