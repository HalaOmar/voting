const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser')
const authController = require('../../auth/web_app/auth_controller');
const celebrityController = require('./celebrity_controller.js');
const uploadController = require('../../commons/upload_middleware');

router.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    celebrityController.listCelebrities)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    celebrityController.registerCelebrity);

router.route('/total')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    celebrityController.getCelebritiesCount);

router.route('/:celebrity_id')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    celebrityController.getById)
  .put(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    celebrityController.updateCelebrity);

module.exports = router;
