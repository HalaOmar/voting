const moment = require('moment');
const Chance = require('chance');
const util = require('util');
const HashUtil = require('hash-util');
const tokenDao = require('./access_token_dao')

const chance = new Chance();

exports.findToken = function (access_token) {
  return tokenDao.getActiveToken(access_token)
};

exports.createTokenForUser = function(userId) {
  return tokenDao.add({
    access_token: generateToken(userId),
    user_id: userId,
    provider_id: null,
    expire_at: moment().add(12, 'months')
  });
};

exports.createTokenForProvider = function(providerId) {
  return tokenDao.add({
    access_token: generateToken(providerId),
    user_id: null,
    provider_id: providerId,
    expire_at: moment().add(12, 'months')
  });
};

exports.deleteToken = function(access_token) {
  return tokenDao.delete(access_token);
};

function generateToken(userId) {
  const randomKey = chance.string({length: 10});
  const timestamp = Date.now().valueOf();
  return encodeToken(userId, randomKey, timestamp);
}

function encodeToken(userId, randomKey, timestamp) {
  const value = util.format("%s|%s|%s", randomKey, userId, timestamp);
  return HashUtil.sha256(value);
}
