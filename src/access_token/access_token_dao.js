const models = require('../models/index');
const Op = models.Sequelize.Op;
const Token = models.access_tokens;

exports.getActiveToken = function (access_token) {
  return Token.findOne({
    where: {
      access_token: access_token,
      expire_at: {
        [Op.gt]: Date.now()
      }
    }
  })
};

exports.add = async function (token) {
  let dto = {
    access_token: token.access_token,
    user_id: token.user_id,
    provider_id: token.provider_id,
    expire_at: token.expire_at
  };

  let createdToken = await Token.create(dto);
  return Promise.resolve(createdToken.toJSON());
};

exports.delete = function (access_token) {
  return Token.destroy({where: {access_token: access_token}})
};
