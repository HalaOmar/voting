
const userPurchasesDao = require('./user_purchases_dao')
const _idGenerator     = require('../commons/id_generator')
const Error            = require('../commons/errors/exceptions')



exports.addToolsLine  = ( toolsLineObj ) =>{

    let tools_line_id  = _idGenerator.generateId()

    let dta = { 
        user_tools_line_id : tools_line_id , 
        user_id            : toolsLineObj.user_id ,
        total_points       : toolsLineObj.total_points ,
        total_price        : toolsLineObj.total_price

    }

    if(!dta.user_id){
        throw new Error.InvalidInputException("Invalid Input")
    }

    return userPurchasesDao.addToolsLine(dta)
 
 }
 
 exports.deleteToolsLineById  = ( toolsLineId ) =>{
 
     return userPurchasesDao.deleteToolsLineById(toolsLineId)
 }
 
 exports.getAllToolsLines  = ( ) =>{
 
     return userPurchasesDao.getAllToolsLines()
    
 }
 
 exports.getUserToolsLineByUserId  = ( userId ) =>{
 
     return userPurchasesDao.getUserToolsLineByUserId(userId) 
 }
 
 exports.setUserToolsLineTotalPrice = ( toolsLineObj ) => {

    let { userId , toolsLineId ,toolPrice } = toolsLineObj
    if(!userId || !toolPrice || !toolsLineId){
        throw new Error.InvalidInputException("Invalid Input")
    }
 
    return userPurchasesDao.setUserToolsLineTotalPrice(toolsLineObj)
 
 }
 
 exports.getUserToolsLineTotalPrice = ( userId , toolslineId ) => {

    if(!userId || !toolslineId){
        throw new Error.InvalidInputException("Invalid Input")
    }
 
     return userPurchasesDao.getUserToolsLineTotalPrice( userId , toolslineId)
 
 }
 
 exports.getUserTotalInvoice = ( userId ) => {
 
     return  userPurchasesDao.getUserTotalInvoice(userId)
 
 }
 
 exports.getAllUsersTotalInvoice = ( ) => {
 
     return  userPurchasesDao.getUserTotalInvoice()
 }

 exports.getMostRecentToolLine = async ( userId ) =>{

    let user_items_line = await userPurchasesDao.getMostRecentToolsLine(userId)
    user_items_line = user_items_line.map( record => record.toJSON())

    return user_items_line[0]
 }
 
 