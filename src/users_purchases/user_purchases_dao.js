const models = require('../models/index')
const UserPurchases = models.user_purchases
const sequelize     = require('sequelize')


exports.addToolsLine  = ( itemsLine ) =>{

   return UserPurchases.create(itemsLine)


}

exports.deleteToolsLineById  = ( toolsLineId ) =>{

    return UserPurchases.delete(
        {
            where:{

                user_tools_line_id :toolsLineId

            }
        }
    )  
}

exports.getAllToolsLines  = ( ) =>{

    return UserPurchases.findAll({
        include : [ { model : models.users , as : 'user'}]
    })
   
}

exports.getUserToolsLineByUserId  = ( userId ) =>{

    return UserPurchases.getAll({
        where:{
            user_id : userId
        },
        include : [ { model : models.users , as : 'user'}]
    })   
}

exports.setUserToolsLineTotalPrice = async ( purchaseObj ) => {
console.log(` purchaseObj :>> `);
console.log( purchaseObj );
   await UserPurchases.increment('total_price', {
        by: purchaseObj.toolPrice * purchaseObj.quentity,
        where: {
             user_id: purchaseObj.userId ,
             user_tools_line_id : purchaseObj.toolsLineId  }});

    return UserPurchases.increment('total_points', {
        by: purchaseObj.toolPoints * purchaseObj.quentity,
        where: {
             user_id: purchaseObj.userId ,
             user_tools_line_id : purchaseObj.toolsLineId  }})

}

exports.getUserToolsLineTotalPrice = ( userId , toolslineId ) => {

    return UserPurchases.findOne( {
        where :{
            user_id : userId , 
            user_tools_line_id : toolslineId
        },
        include : [ { model : models.users , as : 'user'}]
    })

}

exports.getUserTotalInvoice = ( userId ) => {

    return  UserPurchases.findAll({
	
        attributes: ['*', [sequelize.fn('sum', sequelize.col('user_purchases.total_price')), 'invoice']],
      
        group : ['user_id'],
        where :{
            user_id : userId        
        },      
        include : [ { model : models.users , as : 'user'}]
      
      });

}

exports.getAllUsersTotalInvoice = ( ) => {

    return  UserPurchases.findAll({
	
        attributes: ['*', [sequelize.fn('sum', sequelize.col('user_purchases.total_price')), 'invoice']],     
        group : ['user_id'], 
        order: sequelize.literal('invoice DESC'),    
        include : [ { model : models.users , as : 'user'}]
      
      });

}

exports.getMostRecentToolsLine  = async ( userId ) => {

    return UserPurchases.findAll( {

        limit : 1 ,
        where : { user_id : userId } ,
        order: [ [ 'created_at', 'DESC' ]],
        include : [ { model : models.users , as : 'user'}]
    })
}

