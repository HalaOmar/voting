const models = require('../models');
const States = models.states;

exports.getAllStates = async () => {
  let results = await States.findAll({});
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);

};
