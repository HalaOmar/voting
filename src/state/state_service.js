const stateDao = require('./state_dao');

exports.getAllStates = () => {
  return stateDao.getAllStates();
};