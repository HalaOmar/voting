const express = require('express');
const stateController = require('./state_controller');

const router = express.Router();

router.route('/')
  .get(stateController.getAllStates);


module.exports = router;
