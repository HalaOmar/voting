const stateService = require('../state_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.getAllStates = async (req, res) => {
  try {
    let results = await stateService.getAllStates();
    resWrapper.success(res, results);
  } catch (error) {
    resWrapper.error(res, error);
  }
};