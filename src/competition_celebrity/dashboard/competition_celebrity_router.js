const express = require('express')
const router = express.Router()
const authController = require('../../auth/web_app/auth_controller');
const competitionCelebrityController = require('./competition_celebrity_controller')

router.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionCelebrityController.getAllCompetitionCelebrities)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionCelebrityController.addCompetitionCelebrity)
  .delete(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionCelebrityController.deleteCompetitionCelebrity)


module.exports = router