const i18n = require('i18n');
const competitionCelebrityService = require('../competition_celebrity_service')
const resWrapper = require('../../commons/http_res_wrapper');

exports.addCompetitionCelebrity = async (req, res) => {
  let dto = {
    competition_id: (req.body.competition_id) ? req.body.competition_id : null,
    celebrity_id: (req.body.celebrity_id) ? req.body.celebrity_id : null,
  }
  try {
    let result = await competitionCelebrityService.addCompetitionCelebrity(dto);
    resWrapper.success(res, result);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.deleteCompetitionCelebrity = async (req, res) => {
  let competitionId = (req.body.competition_id) ? req.body.competition_id : null;
  let celebrityId = (req.body.celebrity_id) ? req.body.celebrity_id : null;
  try {
    await competitionCelebrityService.deleteCompetitionCelebrity(competitionId, celebrityId);
    resWrapper.success(res);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getAllCompetitionCelebrities = async (req, res) => {
  let competitionId = (req.query.competition_id) ? req.query.competition_id : null;

  try {
    let results = await competitionCelebrityService.getAllCompetitionCelebrities(competitionId);
    resWrapper.success(res, results);
  } catch (e) {
    resWrapper.error(res, e);
  }
};