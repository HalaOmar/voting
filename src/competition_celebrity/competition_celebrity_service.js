const i18n = require('i18n');
const competitionCelebrityDao = require('./competition_celebrity_doa')
const Errors = require('../commons/errors/exceptions');
const MessageKeys = require('../commons/constants').MessageKeys;

exports.addCompetitionCelebrity = async (competitionDto) => {
  let competitionCelebrity = {
    competition_id: competitionDto.competition_id,
    celebrity_id: competitionDto.celebrity_id,
  };

  if (!competitionCelebrity.competition_id || !competitionCelebrity.celebrity_id) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  let isExist = await competitionCelebrityDao.isExist(competitionCelebrity.competition_id, competitionCelebrity.celebrity_id);
  if (isExist) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await competitionCelebrityDao.addCompetitionCelebrity(competitionCelebrity);

  return competitionCelebrityDao.getByCompetitionIdCelebrityId(competitionCelebrity.competition_id, competitionCelebrity.celebrity_id);
}

exports.deleteCompetitionCelebrity = async (competitionId, celebrityId) => {
  if (!competitionId || !celebrityId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await competitionCelebrityDao.deleteCompetitionCelebrity(competitionId, celebrityId);
  return Promise.resolve();
}

exports.getAllCompetitionCelebrities = async (competitionId) => {
  if (!competitionId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }
  let competitions = await competitionCelebrityDao.getAllCompetitionCelebrities(competitionId);
  return Promise.resolve(competitions)
}