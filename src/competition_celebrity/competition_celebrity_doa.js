const models = require('../models/index')
const CompetitionCelebrity = models.competition_celebrities;

const includeCelebrity = {model: models.celebrities, as: 'celebrity'};

const fullIncludes = [
  includeCelebrity,
];

exports.addCompetitionCelebrity = async (competitionCelebrity) => {
  let dto = {
    competition_id: competitionCelebrity.competition_id,
    celebrity_id: competitionCelebrity.celebrity_id,
  }
  let result = await CompetitionCelebrity.create(dto);
  return Promise.resolve(result.toJSON());
};

exports.getByCompetitionIdCelebrityId = async (competitionId, celebrityId) => {
  let result = await CompetitionCelebrity.findOne({
    where: {
      competition_id: competitionId,
      celebrity_id: celebrityId,
    },
    include: fullIncludes,
  });

  if (result) {
    result = result.toJSON();
  }

  return Promise.resolve(result);
}

exports.isExist = async (competitionId, celebrityId) => {
  let result = await CompetitionCelebrity.findOne({
    where: {
      competition_id: competitionId,
      celebrity_id: celebrityId,
    },
  });

  return Promise.resolve(result !== null);
}

exports.getAllCompetitionCelebrities = async (competitionId) => {
  let results = await CompetitionCelebrity.findAll({
    where: {
      competition_id: competitionId,
    },
    include: fullIncludes,
  });
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
};

exports.deleteCompetitionCelebrity = async (competitionId, celebrityId) => {
  await CompetitionCelebrity.destroy({
    where: {
      competition_id: competitionId,
      celebrity_id: celebrityId,
    }
  });
  return Promise.resolve();
};

