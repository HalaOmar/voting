const express = require('express')
const router  = express.Router()
const user_purchases_tools_controller = require('./user_purchases_tools_controller')
const auth_controller = require('../../auth/web_app/auth_controller')

router.route('/')
.post(auth_controller.authenticate , 
      user_purchases_tools_controller.addToolToMyPurchases)
.get( auth_controller.authenticate  ,
    user_purchases_tools_controller.getAllMyPurchases)
.delete( auth_controller.authenticate ,
    user_purchases_tools_controller.deleteToolFromMyPurchases)
.put( auth_controller.authenticate , 
    user_purchases_tools_controller.updateMyPurchasesLine)

router.route('/purchases')
.post(auth_controller.authenticate , 
    user_purchases_tools_controller.getAllMyPurchases)
.delete( auth_controller .authenticate , 
    user_purchases_tools_controller.deleteToolFromMyPurchases)
.put( auth_controller.authenticate , 
    user_purchases_tools_controller.updateMyPurchasesLine)
    

module.exports = router