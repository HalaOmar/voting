const userPurchasesToolsService = require('../user_purchases_tools_service')
const resWraper = require('../../commons/http_res_wrapper')

exports.addToolToMyPurchases = async ( req , res ) => {

    try{
 
       let toolObject ={

           tool_id : req.body.tool_id ? req.body.tool_id : null , 
           user_id : req.body.user_id ? req.body.user_id : null ,
           quentity: req.body.quentity? req.body.quentity:null ,
           competition_id : req.body.competition_id ? req.body.competition_id :null
       }

       let result = await userPurchasesToolsService.addToolToMyPurchases(toolObject)

       resWraper.success( res , result)
    }catch(e){

       resWraper.error( res , e)

    }
}

exports.getAllMyPurchases    = async ( req , res ) =>{

    try{

        let userId = req.body.user_id ? req.body.user_id : null

        let userPurchases = await userPurchasesToolsService.getAllMyPurchases(userId)

        resWraper.success ( res , userPurchases )

    }catch(e){

        resWraper.error( res , e )

    }
}

exports.deleteToolFromMyPurchases = async ( req , res ) =>{

    try{

        let deleteToolObj = {
            user_id : req.body.user_id ? req.body.user_id : null ,
            tool_id : req.body.tool_id ? req.body.tool_id : null ,
            user_tools_line_id : req.body.user_tools_line_id ? 
                                 req.body.user_tools_line_id :
                                 null

        }
    let deleteOp = await userPurchasesToolsService.deleteToolFromMyPurchases(deleteToolObj)
        resWraper.success( res , deleteOp)
    }catch(e){

        resWraper.error( res , e)

    }
}

exports.updateMyPurchasesLine     = async ( req , res ) => {
    try{

        let updateToolObj = {
            user_id : req.body.user_id ? req.body.user_id : null ,
            tool_id : req.body.tool_id ? req.body.tool_id : null ,
            quentity : req.body.quentity ? req.body.quentity : null ,
            price : req.body.price ? req.body.price : null ,

            user_tools_line_id : req.body.user_tools_line_id ? 
                                 req.body.user_tools_line_id :
                                 null ,

            competition_id     : req.body.competition_id ? 
                                 req.body.competition_id :
                                 null ,
        }

    let result = await userPurchasesToolsService.updateMyPurchasesLine(updateToolObj)
    resWraper.success( res , result)

    }catch(e){
    
    resWraper.error( res , e)

    }
}
