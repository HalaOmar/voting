const user_purchases_tools_dao = require('./user_purchases_tools_dao')
const toolService = require('../competition_tools/competition_tools_service')
const userPurchasesService    = require('../users_purchases/user_purchases_service')
const _idGenerator = require('../commons/id_generator')
const Error = require('../commons/errors/exceptions')



exports.addToolToMyPurchases = async ( purchaseObj ) =>{ 

    let { tool_points , tool_price} = await getToolPointsandPrice(purchaseObj.tool_id)

    let purchase_line_id            = await getToolsLineId(purchaseObj.user_id ,
                                                                    tool_points, 
                                                                    tool_price) 

    let dta = {
        user_tools_line_id : purchase_line_id ,
        tool_id          : purchaseObj.tool_id , 
        competition_id   : purchaseObj.competition_id ,
        points           : tool_points ,
        user_id          : purchaseObj.user_id ,
        price            : tool_price ,
        quentity         : purchaseObj.quentity

    }
    if( !dta.tool_id || !dta.user_tools_line_id  ){

        throw new Error.InvalidInputException("Invalid Input Exception")
    }

    let result = await  user_purchases_tools_dao.addToolToMyPurchases( dta )
    result     = result.toJSON()
    if( result) {

    result = await userPurchasesService.setUserToolsLineTotalPrice({
         userId : result.user_id  ,
         toolsLineId : result.user_tools_line_id ,
         toolPrice : result.price * result.quentity,
         toolPoints : tool_points * result.quentity
    })
    }else{
        result = null
    }

    return Promise.resolve(result)
}

exports.getAllMyPurchases = ( userId ) =>{

    if ( !userId){
        throw new Error.InvalidInputException("Invalid userId")
    }

    return user_purchases_tools_dao.getAllMyPurchases(userId)


}

exports.deleteToolFromMyPurchases = ( deleteToolObj )=> {

    console.log(` req.body :>> `);
    console.log(deleteToolObj);
    let { user_tools_line_id , user_id } = deleteToolObj
    if( !user_tools_line_id || !user_id){
        throw new Error.InvalidInputException("Invalid Input")
    }

   return user_purchases_tools_dao.deleteToolFromMyPurchases(deleteToolObj)

}

exports.updateMyPurchasesLine = async ( updateToolObj) =>{

    let { user_tools_line_id , tool_id , user_id  } = updateToolObj

    if( !user_tools_line_id || !tool_id  || !user_id){
        throw new Error.InvalidInputException("Invalid Input")
    }
    let updateObj = { 
        userId : user_id ,
        toolPrice : updateToolObj.price , 
        toolsLineId : user_tools_line_id ,
        quentity : updateToolObj.quentity ,
        toolPoints : 30
    }

    await user_purchases_tools_dao.updateMyPurchasesLine(updateToolObj)
    return userPurchasesService.setUserToolsLineTotalPrice(updateObj)


}



async function getToolsLineId(userId , points , price) {

    let tools_line_id = null

    let user_tools_line = await userPurchasesService.getMostRecentToolLine(userId)

    if(user_tools_line) {

    let  created_at  = user_tools_line.created_at
    let expired = (Date.now() - created_at) / (1000*60*60)
    expired >= 10 ? tools_line_id = null
                                   : tools_line_id = user_tools_line.user_tools_line_id           
    }

    if(!tools_line_id){

        tools_line_id = _idGenerator.generateId()
        let tool_line = await userPurchasesService.addToolsLine({
            user_id : userId ,
            user_tools_line_id :  tools_line_id ,           
            total_points : points , 
            total_price  :price
        })

        tool_line     = tool_line.toJSON()
        tools_line_id = tool_line.user_tools_line_id

    }

    return Promise.resolve(tools_line_id)
    
}

async function getToolPointsandPrice(toolId) {

    let tool = await toolService.getToolWithId( toolId )
    tool = tool.toJSON()

    return Promise.resolve( {
        tool_points : tool.points ,
        tool_price : tool.price
    })
    
}