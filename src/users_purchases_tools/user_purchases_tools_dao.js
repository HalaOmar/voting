const models             = require('../models/index')
const UserPurchasesTools = models.user_purchases_tools
let fullIncludes = [ { model : models.users , as : 'user'} , 
                     { model : models.tools , as : 'tool'} ,
                     { model : models.user_purchases , as : 'user_purchases'}]

exports.addToolToMyPurchases = ( purchaseObj) =>{

    return UserPurchasesTools.create(purchaseObj)
}

exports.getAllMyPurchases   =   (userId) =>{

    return UserPurchasesTools.findAll({
        where : {
            user_id : userId
        },
        include : fullIncludes

    }
    )
}

exports.deleteToolFromMyPurchases = ( deleteToolObj ) =>{

    return UserPurchasesTools.destroy({
        where : {
            user_id : deleteToolObj.user_id , 
            tool_id : deleteToolObj.tool_id , 
            user_tools_line_id : deleteToolObj.user_tools_line_id

        }
    })


}

exports.updateMyPurchasesLine = ( updateToolObj) =>{

return UserPurchasesTools.create(updateToolObj)

}