const multer = require('multer');
const crypto = require('crypto');
const url = require('url');
const gm = require('gm');
const fs = require('fs');

const errorsHandler = require('./http_res_wrapper');

exports.uploadFiles = function (req, res, next) {

  let path = process.env.FILE_STORAGE_PATH;

  // create dir if not exist
  fs.existsSync(path) || fs.mkdirSync(path);

  let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path)
    },
    filename: function (req, file, cb) {
      getCryptoName(function (name) {
        cb(null, name + '.jpeg');
      })
    }
  });

  let upload = multer({storage: storage}).array('file');
  upload(req, res, function (err) {
    if (err) {
      errorsHandler.handle(500, err, res);
      return;
    }
    next();
  });
};

const getCryptoName = function (callback) {
  crypto.pseudoRandomBytes(16, function (err, raw) {
    callback(raw.toString('hex') + Date.now());
  });
};

exports.getCryptoName = getCryptoName;
