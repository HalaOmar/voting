const { ShopConfigConstants } = require("./constants");
const shopConfigSerivce = require('../shop_configs/shop_config_service');

exports.isLineEnabled = async (shop) => {
  let shopId = shop.shop_id;
  let restaurantLine = 0;
  if (shop.shop_configs && shop.shop_configs[ShopConfigConstants.ENTER_RESTAURANT_ORDER_LINE]) {
    restaurantLine = shop.shop_configs[ShopConfigConstants.ENTER_RESTAURANT_ORDER_LINE];
  } else {
    restaurantLine = await shopConfigSerivce.checkRestaurantLine(shopId);
  }
  return Number(restaurantLine);
}