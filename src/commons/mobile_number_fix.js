const logger = require('../libs/logger');
const fileName = __filename.slice(__dirname.length + 1);

exports.fixMobileNumber = (mobileNumber) => {
  logger.debug(`${fileName}::fixMobileNumber - ${mobileNumber}`);

  mobileNumber = mobileNumber.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
    return d.charCodeAt(0) - 1632;
  });

  mobileNumber = mobileNumber.replace(/\s+/g, '');

  if (mobileNumber.startsWith('5')) {
    mobileNumber = mobileNumber.replace('5', '+9665');
  } else if (mobileNumber.startsWith('05')) {
    mobileNumber = mobileNumber.replace('05', '+9665');
  } else if (mobileNumber.startsWith('9665')) {
    mobileNumber = mobileNumber.replace('9665', '+9665');
  }

  mobileNumber = mobileNumber.replace('+96605', '+9665');

  logger.debug(`${fileName}::fixMobileNumber - Mobile after formatting: ${mobileNumber}`);

  return mobileNumber;
};
