const Upload = require('s3-uploader');
const fsextra = require('fs-extra');
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

const AWS = require('aws-sdk');

const s3 = new AWS.S3({
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET_KEY,
});

AWS.config.update({
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET_KEY,
});

const awsConfigs = {
  path: 'images/',
  region: 'eu-central-1',
  acl: 'public-read',
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET_KEY
};

const s3_avatar_options = {
  aws: awsConfigs,

  cleanup: {
    versions: true,
    original: true
  },

  original: {
    awsImageAcl: 'public-read',
    awsImageExpires: 31536000,
    awsImageMaxAge: 31536000
  },

  versions: [
    {
      maxHeight: 480,
      maxWidth: 480,
      aspect: '1:1',
      format: 'jpeg',
      suffix: '-thumb1',
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    }
  ]
};

const imagesOptions = {
  aws: awsConfigs,

  cleanup: {
    versions: true,
    original: true
  },

  original: {
    awsImageAcl: 'public-read',
    awsImageExpires: 31536000,
    awsImageMaxAge: 31536000
  },

  versions: [
    {
      maxHeight: 480,
      maxWidth: 480,
      aspect: '1:1',
      format: 'png',
      suffix: '-thumb1',
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    },
    {
      maxHeight: 1040,
      maxWidth: 1040,
      format: 'png',
      suffix: '-large',
      quality: 80,
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    }
  ]
};

const s3_ads_options = {
  aws: awsConfigs,

  cleanup: {
    versions: true,
    original: true
  },

  original: {
    awsImageAcl: 'public-read',
    awsImageExpires: 31536000,
    awsImageMaxAge: 31536000
  },

  versions: [
    {
      maxHeight: 1110,
      maxWidth: 1110,
      format: 'jpeg',
      suffix: '-large1',
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    },
    {
      maxHeight: 470,
      format: 'jpeg',
      suffix: '-large2',
      // quality: 80,
      aspect: '3:2!h',
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    },
    {
      maxWidth: 780,
      aspect: '3:2!h',
      format: 'jpeg',
      suffix: '-medium',
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    },
    {
      maxWidth: 320,
      aspect: '16:9!h',
      format: 'jpeg',
      suffix: '-small'
    }]
};

const qr_image_options = {
  aws: awsConfigs,

  cleanup: {
    versions: true,
    original: true
  },

  original: {
    awsImageAcl: 'public-read',
    awsImageExpires: 31536000,
    awsImageMaxAge: 31536000
  },

  versions: [
    {
      maxHeight: 480,
      maxWidth: 480,
      aspect: '1:1',
      format: 'jpeg',
      suffix: '-thumb1',
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    },
    {
      maxHeight: 1040,
      maxWidth: 1040,
      format: 'png',
      suffix: '-large',
      quality: 80,
      awsImageExpires: 31536000,
      awsImageMaxAge: 31536000
    }
  ]
};

exports.uploadAdsPhoto = async (filePath) => {
  console.log('uploadAdsPhoto');
  try {
    let folder = process.env.S3_ADS_FOLDER;
    let result = await createUploadPromise(filePath, folder, imagesOptions);
    return Promise.resolve({s3_id: result})
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};


exports.uploadIcons = async (filePath, storeFolder) => {
  console.log('uploadPlacePhoto');
  try {
    let result = await uploadFile(filePath, storeFolder);
    return Promise.resolve({s3_id: result})
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};

exports.uploadShopPhoto = async (filePath) => {
  console.log('uploadPlacePhoto');
  try {
    let folder = process.env.S3_SHOP_FOLDER;
    let result = await createUploadPromise(filePath, folder, imagesOptions);
    return Promise.resolve({s3_id: result})
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};

exports.uploadAvatar = async (filePath) => {
  console.log('uploadAvatar');
  try {
    let folder = process.env.S3_USERS_FOLDER;
    let result = await createUploadPromise(filePath, folder, s3_avatar_options);
    return Promise.resolve({s3_id: result});
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};

exports.uploadQrImage = async (filePath) => {
  console.log(`uploadQrImage - filePath: ${filePath}`);
  try {
    let folder = process.env.S3_USERS_FOLDER;
    let result = await createUploadPromise(filePath, folder, qr_image_options);
    return Promise.resolve({s3_id: result})
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};

exports.uploadProviderAvatar = async (filePath) => {
  console.log('uploadAvatar');
  try {
    let folder = process.env.S3_PROVIDERS_FOLDER;
    let result = await createUploadPromise(filePath, folder, s3_avatar_options);
    return Promise.resolve({s3_id: result});
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};

exports.uploadServiceIcon = async (filePath) => {
  try {
    let folder = process.env.S3_SERVICES_FOLDER;
    let result = await createUploadPromise(filePath, folder, s3_avatar_options);
    return Promise.resolve({s3_id: result})
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
};


exports.uploadApkFiles = async (appId, filePath, fileName) => {
  let bucket = process.env.S3_BUCKET + '/' + process.env.S3_APKS_FOLDER.replace('/', '');
  console.log('bucket: ' + bucket);
  console.log('filePath: ' + filePath);
  console.log('fileName: ' + fileName);

  let body = await readFile(filePath);
  let params = {
    Bucket: bucket,
    Key: fileName,
    Body: body,
    ACL: 'public-read'
  };

  let result = await new AWS.S3().putObject(params).promise();

  console.log(result);
  console.log(fileName);

  return Promise.resolve(fileName);
};


const createUploadPromise = (imagePath, destinationPath, mOptions) => {
  console.log("S3 upload image path ", imagePath);
  console.log("S3 destination image path ", destinationPath);
  console.log("process.env.S3_BUCKET ", process.env.S3_BUCKET);

  return new Promise((resolve, reject) => {

    const client = new Upload(process.env.S3_BUCKET, mOptions);
    mOptions.aws.path = destinationPath;

    client.upload(imagePath, {}, (err, versions, meta) => {
      if (err) {
        console.log("S3 upload error: ", err);
        deleteFile(imagePath);
        reject(err);
      } else {
        console.log("Versions", versions);
        console.log("Meta", meta);

        let s3Name = null;

        if (versions) {
          for (let i = 0; i < versions.length; ++i) {
            if (versions[i].original) {
              s3Name = versions[i].key.split('/').pop().replace(/\.[^/.]+$/, "");
              console.log('File URL: ' + versions[i].url);
              console.log("S3: ", s3Name);
              break;
            }
          }
        }

        console.log("S3 ID: ", s3Name);
        resolve(s3Name);
      }
    })
  });
};

const uploadFile = (fileName, storeFolder) => {
  
  return new Promise((resolve,reject) => {
    const fileContent = fs.readFileSync(fileName);
    let delemiter = '\\';
    let folder = storeFolder || process.env.S3_SHOP_FOLDER;
    if(fileName.indexOf(delemiter) === -1){
      delemiter = '/';
    }
    let temp = fileName.split(delemiter);
    let key = folder + temp[temp.length-1];
    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.S3_BUCKET,
        Body: fileContent,
        Key: key,
        ACL: 'public-read',
        ContentType: 'image/png',
    };
  
    // Uploading files to the bucket
    s3.upload(params, function(err, data) {
        if (err) {
            return reject(err);
        }
        let tempLoc = data.Location.split('/');
        let temps3Id = tempLoc[tempLoc.length-1].split('.');
        resolve(temps3Id[0]);
    });

  })
};

const deleteFile = (imagePath) => {
  fsextra.remove(imagePath, (err) => {
    if (err) {
      console.log("Error on removing Image: ", err);
    } else {
      console.log("Image Deleted: ", imagePath);
    }
  });
};
