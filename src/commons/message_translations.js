const i18n = require('i18n');
const { Locales } = require('./constants');

exports.getMessageTranslations = function (messageKey, parameters = {}) {

  let args = {};

  args = getMessageArgs(parameters);
  let message = i18n.__(messageKey, args);


  args = getMessageArgs(parameters, Locales.ENGLISH);
  let message_en = i18n.__({ phrase: messageKey, locale: Locales.ENGLISH }, args);

  args = getMessageArgs(parameters, Locales.ARABIC);
  let message_ar = i18n.__({ phrase: messageKey, locale: Locales.ARABIC }, args);

   return {
     notification_type: null,
     message,
     message_ar,
     message_en
   }

};

exports.getAttributeTranslation = function(object, attribute){
  return { ar: object[`${attribute}_ar`], en: object[`${attribute}_en`] }
}

getMessageArgs = function (parameters, local = i18n.getLocale()) {
  let args = {};
  
  let keys = Object.keys(parameters);
  
  keys.forEach(key => {
    if(typeof parameters[key] === 'object'){
      args[key] = parameters[key][local];
    } else{
      args[key] = parameters[key]
    }
  });

  return args;
}

