const fs = require('fs');
const path = require('path');

exports.prepareQuery = async (query_name) => {

  if(query_name){
    let file_name = path.normalize(`${__dirname}/sql_queries/${query_name}.sql`);
    if (fs.existsSync(file_name)) { 
        return  fs.readFileSync(file_name, 'utf8')
      } 
  }

};
