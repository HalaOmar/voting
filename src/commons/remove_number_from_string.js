const logger = require('../libs/logger');


exports.removeNumbers = (stringValue) => {

  let reg = /\+|[0-9]/g;
  let s = stringValue.replace(reg, '');

  if(s && s.length > 0){
    return s.trim();
  }else{
    return null;
  }

};
