const request = require('request');
const logger = require('./logger');
const util = require('util');

exports.send = function (userIds, receivers, title, body) {
  const key = process.env.FIREBASE_KEY;
  logger.info(`Send Push Notification to: ${userIds}, Title: ${title}, Body: ${body}`);

  return new Promise((resolve, reject) => {
    if (typeof receivers == "string") {
      receivers = [receivers];
    }

    let options = {
      method: 'POST',
      url: 'https://fcm.googleapis.com/fcm/send',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=' + key
      },
      body: {
        "registration_ids": receivers,
        "priority": "high",
        "data": {
          "title": title,
          "body": body,
        },
        "notification": {
          "title": title,
          "body": body,
        },
      },
      json: true
    };

    logger.debug("Push Message: " + util.inspect(options, false, null));

    request(options, function (err, response, body) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        console.log(body);
        resolve();
      }
    });
  })
};


exports.sendToTopic = function (topic, body, title) {
  const key = process.env.FIREBASE_KEY;
  return new Promise((resolve, reject) => {
    let options = {
      method: 'POST',
      url: 'https://fcm.googleapis.com/fcm/send',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=' + key
      },
      body: {
        "to": '/topics/' + topic,
        "priority": "high",
        "notification": {
          "body": body,
          "title": title
        },
        "data": {
          "body": body,
          "title": title,
        },
      },
      json: true
    };

    logger.debug("Broadcast: " + util.inspect(options, false, null));

    request(options, function (err, response, body) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        console.log(body);
        resolve();
      }
    });
  });

};
