const models = require('../models/index');
const Roles = require("../commons/constants").Roles;
const fcm = require('./fcm');
const apn = require('./apnNotifications');
const logger = require('./logger');
const i18n = require('i18n');
const DeviceTokensService = require('../device_token/device_token_service');

const User = models.users;
const Order = models.orders;

const notifyAdmins = function (msg, order) {
  logger.debug(" Notify admins, msg", msg);
  // Find users
  User.all({
    attributes: ['user_id'],
    where: {
      role_id: [Roles.SUPERVISOR, Roles.ADMIN]
    }
  })
    .then(function (users) {
      let data = extractDataMessage(msg, order);
      push(users, data);
    })
    .catch((e) => {
      console.log(e);
    })
};

exports.notifyOwner = async (userId, msg, order) => {
  logger.debug(" Notify Order Owner orderId", order.order_id);

  try {
    let users = [{user_id: userId}];
    let data = extractDataMessage(msg, order);
    push(users, data);
  } catch (error) {
    logger.error(error);
  }
};

const extractDataMessage = (msg, order) => {
  const data = {
    notification_type: "notification_message",
    notification_title: 'Saudi App Store',
    notification_body: msg
  };

  if (order) {
    data.notification_type = "order_status_update";
    data.order_id = order.order_id;
    data.user_id = order.user_id;
    data.status_id = order.status_id;
    data.created_at = order.created_at;
    data.updated_at = order.updated_at;
    data.order_id = order.order_id;
  }

  return data;
};

const push = (users, message) => {
  let ids = [];
  for (let i = 0; i < users.length; ++i) {
    ids.push(users[i].user_id)
  }

  let androidTokens = [];
  let iOSTokens = [];

  console.log('push to #' + ids.length + ' Devices');

  // Get users access_tokens

  DeviceTokensService.getUsersDeviceTokens(ids)
    .then((deviceTokens) => {
      // Split device access_tokens
      for (let i = 0; i < deviceTokens.length; ++i) {
        if (deviceTokens[i].token_type == "IOS") {
          iOSTokens.push(deviceTokens[i].device_token);
        } else {
          androidTokens.push(deviceTokens[i].device_token);
        }
      }

      if (androidTokens.length > 0) {
        fcm.send(androidTokens, message)
          .then((result) => {
            // TODO: get invalid access_tokens from fcm
            // console.log('Invalid Firebase tokens: ' + result.invalid_tokens);
            // if (result.invalid_tokens.length > 0) {
            //   NotificationToken.destroy({where: {device_token: result.invalid_tokens}});
            // }
          })
          .catch((e) => {
            console.log(e);
          })
      }

      if (iOSTokens.length > 0) {
        fcm.send(iOSTokens, message)
          .then((result) => {
            // console.log('Invalid APN tokens size: ' + result.invalid_tokens);
            // if (result.invalid_tokens.length > 0) {
            //   NotificationToken.destroy({where: {device_token: result.invalid_tokens}});
            // }
          })
          .catch((e) => {
            console.log(e);

          })
      }
    })
    .then(() => {
      console.log('Push completed');
    })
    .catch((e) => {
      console.log(e);
    });
};

exports.notifyAdmins = notifyAdmins;
exports.push = push;
