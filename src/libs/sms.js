const logger = require('./logger');
const unifonic = require('./sms_provider/unifonic');

exports.send = async (mobile, msg) => {
  logger.info(`Send SMS - mobile: ${mobile}, msg: ${msg}`);

  try {
   await unifonic.send(mobile, msg)
  } catch (e) {
    logger.error(`Send SMS error: ${e}`);
  }
};
