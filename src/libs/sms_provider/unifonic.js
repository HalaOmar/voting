const request = require('request');
const logger = require('../logger');
const appSid = process.env.SMS_UNIFONIC_SID;
const url = 'http://basic.unifonic.com/rest/SMS/messages';
const senderId = process.env.SMS_UNIFONIC_SENDER;
const configDao = require('../../configs/config_dao');

const send = async (phoneNumber, message) => {

  let config = await configDao.getByKey('ENABLE_SMS_GATEWAY');
  if (config) {
    logger.info(`'configs[ENABLE_SMS_GATEWAY] = ${config.value}`);
    if (config.value === 'false') {
      return Promise.resolve(200);
    }
  }

  return new Promise((resolve, reject) => {
    // remove + from numbers
    phoneNumber = phoneNumber.replace(/[^\d,]/g, '');

    let data = {
      AppSid: appSid,
      Recipient: phoneNumber,
      Body: message,
      SenderID: senderId,
      responseType: 'JSON',
    };

    logger.info(`Message: ${message}`);

    request.post({
      url: url,
      headers: {
        "Authorization": "Basic ZWxtbW9iaWxlYXBwczphaG1hZGVzc2FtMjAxMQ==",
        "Accept": "application/json",
      },
      form: data
    }, (err, httpResponse, body) => {
      if (err) {
        logger.error(err);
        reject(err);
        return;
      }

      logger.debug(`Response: ${httpResponse.statusCode}`);
      logger.debug(`Body: ${body}`);

      let jsonBody = JSON.parse(body);
      if (jsonBody.success) {
        resolve();
      } else {
        logger.error(jsonBody.message);
        reject(jsonBody.message);
      }
    })
  })
};

exports.send = send;
