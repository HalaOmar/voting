const QRCode = require('qrcode');
const i18n = require('i18n');
const logger = require('./logger');

exports.generateQR = async (userId) => {
  let path = '/tmp/' + userId + '_qr.jpg';
  try {
    await QRCode.toFile(path, userId, {
      version: 8
    });
    logger.info(`QR generated Successfully: ${path}`);
    return Promise.resolve(path);
  } catch (err) {
    logger.error(`QR generated Failed: ${err}`);
    return Promise.resolve(null);
  }
};
