const apn = require('apn');
const util = require('util');
const logger = require('./logger');

const bundleId = 'com.skipro';
const cerrificate = __dirname + '/certificates/prodApn.p12';
const passphrase = '123';

exports.send = (deviceTokens, payload) => {
  logger.debug(" APN Called ")
  return new Promise( (resolve, reject) => {
    let options = {
      pfx: cerrificate,
      passphrase: passphrase,
      production: true // Set to true if sending a notification to a production iOS app
    };

    let apnProvider = new apn.Provider(options);

    let notification = new apn.Notification();
    notification.alert = payload.notification_title;
    notification.topic = bundleId;
    notification.payload = payload;

    logger.debug('>>>>>>>>> Push APN notification' + util.inspect(notification, false, null));

    apnProvider.send(notification, deviceTokens).then( (result) => {
      logger.debug('APN result:', util.inspect(result, false, null));

      let invalid_tokens = [];
      let devices = result.failed;

      for (let i=0; i<devices.length; ++i) {
        let device = devices[i].device;
        invalid_tokens.push(device);
      }

      result.invalid_tokens = invalid_tokens;

      apnProvider.shutdown();

      resolve(result);
    });
  })
};
