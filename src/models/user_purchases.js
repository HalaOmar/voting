'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserPurchases = sequelize.define('user_purchases', {

    user_tools_line_id : {
        type : DataTypes.STRING ,
        primaryKey : true
    } ,
    user_id : { 
        type: DataTypes.STRING,
        references: {
          model: 'users',
          key: 'user_id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
    },
    total_points : {
        type : DataTypes.INTEGER,
        defaultValue : 0
    },
    total_price : {
      type : DataTypes.INTEGER ,
      defaultValue : 0
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  UserPurchases.associate = function (models) {  
    //The tool belongs to one and only  one user
    UserPurchases.belongsTo(models.users, {as: 'user', foreignKey: 'user_id'});

  };

  return UserPurchases;
};