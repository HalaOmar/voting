module.exports = function (sequelize, DataTypes) {
  const DeviceToken = sequelize.define('device_tokens', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    user_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'users',
        key: 'user_id'
      },
      onUpdate: 'cascade',
      onDelete: 'restrict'
    },
    device_token: {
      type: DataTypes.STRING,
    },
    token_type: {
      type: DataTypes.ENUM('ANDROID', 'IOS', 'WEB'),
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  DeviceToken.prototype.toJSON = function () {
    let values = this.get();

    delete values.created_at;
    delete values.updated_at;

    return values;
  };

  DeviceToken.associate = function (models) {
    DeviceToken.belongsTo(models.users, {as: 'user', foreignKey: 'user_id'});
  };

  return DeviceToken;
};
