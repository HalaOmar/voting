const moment = require('moment');

module.exports = function (sequelize, DataTypes) {

  let User = exports.userSchema = sequelize.define('users', {
    user_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    full_name: {
      type: DataTypes.STRING
    },
    mobile: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
    },
    avatar_image: DataTypes.STRING,
    s3_id: DataTypes.STRING,
    blocked: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    gender: DataTypes.INTEGER,
    date_of_birth: DataTypes.DATEONLY,
    role_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  User.prototype.toJSON = function () {
    let values = this.get();

    if (values['s3_id']) {
      let url = process.env.S3_DOMAIN + process.env.S3_USERS_FOLDER + values['s3_id'] + '-thumb1.jpeg'
      values['image_url'] = url
    } else {
      values['image_url'] = null
    }

    if(values['date_of_birth']){
      let duration = moment.duration(moment().diff(values['date_of_birth']));
      values['age'] = Math.trunc(duration.asYears());
    }else{
      values['age'] = null;
    }

    delete values.avatar_image;
    delete values.s3_id;
    delete values.created_at;
    delete values.updated_at;
    return values;
  };

  User.prototype.toPublicJSON = function () {
    let values = this.get();

    if (values['s3_id']) {
      let url = process.env.S3_DOMAIN + process.env.S3_USERS_FOLDER + values['s3_id'] + '-thumb1.jpeg'
      values['image_url'] = url
    } else {
      values['image_url'] = null
    }

    return {
      user_id: values.user_id,
      full_name: values.full_name,
      image_url: values.image_url,
    };
  };

  User.associate = function (models) {
    User.belongsTo(models.roles, {foreignKey: 'role_id', allowNull: false});

  };

  return User;
};
