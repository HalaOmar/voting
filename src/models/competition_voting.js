'use strict';
module.exports = (sequelize, DataTypes) => {

  const CompetitionVoting = sequelize.define('competition_votings', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    competition_id: {
      type: DataTypes.STRING,
      references: {
        model: 'competitions',
        key: 'competition_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    user_id: {
      type: DataTypes.STRING,
      references: {
        model: 'users',
        key: 'user_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    celebrity_id: {
      type: DataTypes.STRING,
      references: {
        model: 'celebrities',
        key: 'celebrity_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  CompetitionVoting.associate = function (models) {

     CompetitionVoting.belongsTo(models.users, {foreignKey:'user_id' });
     CompetitionVoting.belongsTo(models.celebrities, {foreignKey : "celebrity_id" });
     CompetitionVoting.belongsTo(models.competitions, {foreignKey : "competition_id" });

  };

  return CompetitionVoting;
};