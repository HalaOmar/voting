'use strict';
module.exports = (sequelize, DataTypes) => {
  const CompetitionTools = sequelize.define('competition_tools', {
    competition_id: {
      type: DataTypes.STRING,
      references: {
        model: 'competitions',
        key: 'competition_id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    },
    tool_id: {
      primaryKey: true,
      type: DataTypes.STRING,
      references: {
        model: 'tools',
        key: 'tool_id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  CompetitionTools.associate = function (models) {
     CompetitionTools.belongsTo(models.competitions , {as:"competition", foreignKey : 'competition_id'});
     CompetitionTools.belongsTo(models.tools , {as:"tool",foreignKey : 'tool_id'});
  };

  return CompetitionTools;
};