'use strict';

module.exports = (sequelize, DataTypes) => {
  const Competition = sequelize.define('competitions', {
    competition_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    started_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    ended_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  Competition.associate = function (models) {
    // associations can be defined here
    
  };

  return Competition;
};