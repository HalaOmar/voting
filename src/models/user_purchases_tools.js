'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserToolsLines = sequelize.define('user_purchases_tools', {
    id:{
      type: DataTypes.INTEGER ,
      autoIncrement : true , 
      primaryKey : true
    },
    competition_id: {
      type: DataTypes.STRING,
      references: {
        model: 'competitions',
        key: 'competition_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    tool_id: {
        type: DataTypes.STRING,
        references: {
          model: 'tools',
          key: 'tool_id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
    },
    user_tools_line_id : {
        type : DataTypes.STRING ,
        references : {
            model : 'user_purchases' , 
            key   : 'user_tools_line_id'        
        } ,
        onDelete  : 'cascade' ,
        onUpdate  : 'cascade'
    } ,
    quentity : {
        type : DataTypes.INTEGER , 
        allowNull :false
    } ,
    price    :{
        type : DataTypes.INTEGER(8,10)
    } ,
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  UserToolsLines.associate = function (models) {
    
    //The tool belongs to one and only  one user
    UserToolsLines.belongsTo(models.users, {as: 'user', foreignKey: 'user_id'});
    UserToolsLines.belongsTo(models.tools, {as: 'tool', foreignKey: 'tool_id'});

    UserToolsLines.belongsTo(models.user_purchases, {as: 'user_purchases',
     foreignKey: 'user_tools_line_id'});

  };

  return UserToolsLines;
};