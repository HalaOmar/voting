module.exports = function (sequelize, DataTypes) {
  const MobileCodes = sequelize.define('mobile_codes', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: false
    },
    code: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    code_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1, // CLIENT APP CODE
    },
    expire_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    used: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  MobileCodes.prototype.toJSON = function () {
    let values = this.get();

    delete values.created_at;
    delete values.updated_at;

    return values;
  };

  MobileCodes.associate = function (models) {};

  return MobileCodes;
};
