'use strict';
module.exports = (sequelize, DataTypes) => {
  const CompetitionCelebrity = sequelize.define('competition_celebrities', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    competition_id: {
      primaryKey: true,
      type: DataTypes.STRING,
      references: {
        model: 'competitions',
        key: 'competition_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    celebrity_id: {
      primaryKey: true,
      type: DataTypes.STRING,
      references: {
        model: 'celebrities',
        key: 'celebrity_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  CompetitionCelebrity.associate = function (models) {
    // CompetitionCelebrity.belongsTo(models.celebrities, {foreignKey: 'celebrityId'});
    // CompetitionCelebrity.hasMany(models.competitions , { foreignKey :'competitionId'})
  };

  return CompetitionCelebrity;
};