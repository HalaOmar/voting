'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserPoints = sequelize.define('user_points', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.STRING,
      references: {
        model: 'users',
        key: 'user_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    purchase_id: {
      type : DataTypes.STRING
    },
    points : {
      type:DataTypes.INTEGER ,
      defaultValue:0
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  UserPoints.associate = function (models) {
    UserPoints.belongsTo(models.users, {as: 'user', foreignKey: 'user_id'});
    // UserPoints.belongsTo(models.purchases,{as: 'purchase', foreignKey: 'purchase_id'});
  };

  return UserPoints;
};