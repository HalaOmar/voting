
module.exports = function(sequelize, DataTypes) {
  const PendingUser = sequelize.define('pending_users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  PendingUser.prototype.toJSON = function () {
    let values = this.get();

    delete values.created_at;
    delete values.updated_at;

    return values;
  };

  PendingUser.associate = function (models) {};

  return PendingUser;
};
