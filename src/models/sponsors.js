'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sponsor = sequelize.define('sponsors', {
    sponsor_id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    s3_id: DataTypes.STRING,
    mobile: DataTypes.STRING,
    url_link: DataTypes.STRING,
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});
  Sponsor.associate = function (models) {
    // associations can be defined here
  };
  return Sponsor;
};