'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tool = sequelize.define('tools', {
    tool_id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    tool_name : { 
      type : DataTypes.STRING
    },
    points:{ 
      allowNull: false,
      type :DataTypes.INTEGER,
      defaultValue: 0
    },
    price: {
      allowNull: false,
      type :DataTypes.INTEGER,
      defaultValue: 0

    },
    s3_id: {
      allowNull: false,
      type :DataTypes.STRING
    },
    competition_id : {
        type : DataTypes.STRING ,
        references : {
            model : 'competitions' ,
            key   :'competition_id'
        } ,
        onDelete  :'cascade',
        onUpdate  :'cascade'
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  Tool.associate = function (models) {

    Tool.belongsTo(models.competitions,{ as:'competition' , foreignKey : 'competition_id'})

    
  };

  return Tool;
};