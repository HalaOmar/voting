const i18n = require('i18n');

module.exports = function (sequelize, DataTypes) {
  const Config = sequelize.define('configs', {
    key: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING
    },
    value: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  Config.prototype.toJSON = function () {
    let values = this.get();
    delete values.created_at;
    delete values.updated_at;
    return values;
  };

  Config.associate = function (models) {
  };


  return Config;
};
