const i18n = require('i18n');

module.exports = function (sequelize, DataTypes) {
  const City = sequelize.define('cities', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name_ar: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name_en: {
      type: DataTypes.STRING,
      allowNull: false
    },
    state_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'states',
        key: 'id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  City.prototype.toJSON = function () {
    let values = this.get();
    values['name'] = (i18n.getLocale() === 'en') ? values['name_en'] : values['name_ar'];

    delete values.name_en;
    delete values.name_ar;
    delete values.created_at;
    delete values.updated_at;

    return values;
  };

  City.associate = function (models) {
  };


  return City;
};
