'use strict';
module.exports = (sequelize, DataTypes) => {
  const CompetitionSponsor = sequelize.define('competition_sponsors', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    competition_id: {
      primaryKey: true,
      type: DataTypes.STRING,
      references: {
        model: 'competitions',
        key: 'competition_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    sponsor_id: {
      primaryKey: true,
      type: DataTypes.STRING,
      references: {
        model: 'sponsors',
        key: 'sponsor_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  CompetitionSponsor.associate = function (models) {
    CompetitionSponsor.belongsTo(models.sponsors, {as: 'sponsor', foreignKey: 'sponsor_id'});
    CompetitionSponsor.belongsTo(models.competitions,{as: 'competition', foreignKey: 'competition_id'});
  };

  return CompetitionSponsor;
};