module.exports = function (sequelize, DataTypes) {

  const AccessToken = sequelize.define('access_tokens', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    user_id: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'users',
        key: 'user_id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
    },
    access_token: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    expire_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }
  }, {});

  AccessToken.prototype.toJSON = function () {
    let values = this.get();

    delete values.created_at;
    delete values.updated_at;

    return values;
  };

  AccessToken.associate = function (models) {
    AccessToken.belongsTo(models.users, {foreignKey: 'user_id', allowNull: false});
  };

  return AccessToken
};
