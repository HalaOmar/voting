const i18n = require('i18n');

module.exports = function (sequelize, DataTypes) {
  const State = sequelize.define('states', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name_ar: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name_en: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
  }, {});

  State.prototype.toJSON = function () {
    let values = this.get();
    values['name'] = (i18n.getLocale() === 'en') ? values['name_en'] : values['name_ar'];

    delete values.name_en;
    delete values.name_ar;
    delete values.created_at;
    delete values.updated_at;

    return values;
  };

  State.associate = function (models) {
  };


  return State;
};
