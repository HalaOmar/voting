'use strict';
const i18n = require("i18n");
module.exports = (sequelize, DataTypes) => {
  const Celebrity = sequelize.define('celebrities', {
    celebrity_id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    full_name: DataTypes.STRING,
    mobile: DataTypes.STRING,
    s3_id: DataTypes.STRING,
    gender: DataTypes.INTEGER,
    social_links: DataTypes.TEXT,
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }

  }, {});

  Celebrity.prototype.toJSON = function () {
    let values = this.get();

    if (values['social_links']) {
      values['social_links'] = JSON.parse(values['social_links']);
    }

    return values;
  };

  Celebrity.associate = function (models) {

    
  };
  return Celebrity;
};