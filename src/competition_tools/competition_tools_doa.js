const models = require('../models/index')
const Tool = models.tools;
const _idGenerator = require('../commons/id_generator');
let toolId 
let fullIncludes   = [ { model : models.competitions , as :"competition"}]

exports.addCompetitionTool = async (competitionTool) => {

   toolId =  _idGenerator.generateId()
  let dto = {
    tool_id       :   toolId                    ,
    tool_name     :   competitionTool.tool_name ,
    points        :   competitionTool.tool_points ,
    price         :   competitionTool.tool_price ,
    s3_id         :   competitionTool.s3_id,
    competition_id:   competitionTool.competition_id   
  }
  
    let tool = await Tool.create(dto);
    return Promise.resolve( tool );
};


exports.isExist = async (toolId) => {
  let result = await Tool.findOne({
    where: {
      tool_id: toolId
    },
  });

  return Promise.resolve(result !== null);
}

exports.getAllToolsByCompetitionId = async (competitionId) => {
  let results = await Tool.findAll({
    where: {
      competition_id: competitionId,
    },
    include: fullIncludes,
  });
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
};

exports.deleteCompetitionTool =  (toolId) => {
  return Tool.destroy({
    where: {
      tool_id: toolId
    }
  });
  
};

exports.updateCompetitionTool = async (toolId, tool) => {
  await Tool.update( tool , {
    where: {
      tool_id: toolId,
    }
  });
  return Promise.resolve();
};

exports.getAllTools = ( ) =>{

  return Tool.findAll( {
    include :[ { model : models.competitions , as : 'competition'}]
  })

}

exports.getToolWithId = ( toolId ) =>{

  return Tool.findOne({
    where:{
      tool_id : toolId
    }
  })
}