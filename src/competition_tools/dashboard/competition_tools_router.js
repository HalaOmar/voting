const express = require('express')
const router = express.Router()
const authController = require('../../auth/web_app/auth_controller');
const competitionToolsController = require('./competition_tools_controller')
const uploadController = require('../../commons/upload_middleware');

router.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionToolsController.getAllTools)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    competitionToolsController.addCompetitionTool)
  .delete(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionToolsController.deleteCompetitionTool)

router.route('/:toolId')
  .put(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    competitionToolsController.updateCompetitionTool)
  
router.route('/:competition_id')
.get( authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    competitionToolsController.getAllToolsByCompetitionId)



module.exports = router