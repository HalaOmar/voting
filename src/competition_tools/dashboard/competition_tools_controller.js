const i18n = require('i18n');
const competitionToolsService = require('../competition_tools_service')
const resWrapper = require('../../commons/http_res_wrapper');

exports.addCompetitionTool = async (req, res) => {
  
  let dto = {
    competition_id: (req.body.competition_id) ? req.body.competition_id : null,
    tool_name: (req.body.tool_name) ? req.body.tool_name : null,
    tool_points: (req.body.tool_points) ? req.body.tool_points : null,
    tool_price: (req.body.tool_price) ? req.body.tool_price : null,
    tool_image_path: req.files ? req.files[0].path :null
  }
  try {
    let result = await competitionToolsService.addCompetitionTool(dto)
    resWrapper.success(res, result);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getAllTools           = async (req ,res) => {

  try{
    let tools = await competitionToolsService.getAllTools()
    resWrapper.success(res , tools)

  }catch(e){
    resWrapper.error(res , e)
  }
}

exports.deleteCompetitionTool = async (req, res) => {
  
  // let competitionId = (req.body.competition_id) ? req.body.competition_id : null;
  let toolId = (req.body.tool_id) ? req.body.tool_id : null;
  try {
    let done = await competitionToolsService.deleteCompetitionTool(toolId);  
    resWrapper.success(res , `done ${done}`);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getAllToolsByCompetitionId = async (req, res) => {

  let competitionId = (req.params.competition_id) ? req.params.competition_id : null;

  try {
    let results = await competitionToolsService.getAllToolsByCompetitionId(competitionId);
     resWrapper.success(res, results);
  } catch (e) {
     resWrapper.error(res, e);
  }
};

exports.updateCompetitionTool = async (req, res) => {
 
  let toolId = (req.params.toolId) ? req.params.toolId : null;
  let tool = {
    competition_id : (req.body.competition_id) ? req.body.competition_id : null,
    tool_name: (req.body.tool_name) ? req.body.tool_name : null,
    tool_points: (req.body.tool_points) ? req.body.tool_points : null,
    tool_price: (req.body.tool_price) ? req.body.tool_price : null,
    tool_image: req.files ? req.files[0].path :null
  }
  try {
    await competitionToolsService.updateCompetitionTool(toolId, tool);
    resWrapper.success(res);
  } catch (e) {
    resWrapper.error(res, e);
  }
};