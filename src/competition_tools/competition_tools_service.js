const i18n = require('i18n');
const competitionToolsDao = require('./competition_tools_doa')
const Errors = require('../commons/errors/exceptions');
const MessageKeys = require('../commons/constants').MessageKeys;
const s3Uploader = require('../commons/s3Uploader')

exports.addCompetitionTool = async (competitionDto) => {
  let competitionTool = {
    competition_id: competitionDto.competition_id,
    tool_name: competitionDto.tool_name,
    tool_points: competitionDto.tool_points,
    tool_price: competitionDto.tool_price,
    tool_image_path:competitionDto.tool_image_path
  };

  if (!competitionTool.competition_id || !competitionTool.tool_image_path) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  if(competitionDto.tool_image_path){

      let result = await s3Uploader.uploadAvatar(competitionTool.tool_image_path);
      competitionTool.s3_id = result.s3_id;
  }

  return competitionToolsDao.addCompetitionTool(competitionTool)

  // return competitionToolsDao.getAllToolsByCompetitionId(competitionTool.competition_id)

}

exports.deleteCompetitionTool =  (toolId) => {
  if (!toolId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  return competitionToolsDao.deleteCompetitionTool(toolId)
  
}

exports.getAllToolsByCompetitionId = async (competitionId) => {
  if (!competitionId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }
  let tools = await competitionToolsDao.getAllToolsByCompetitionId(competitionId)
  return Promise.resolve(tools)
}

exports.updateCompetitionTool = async (toolId , toolDto) => {

  let competitionTool = {
    competition_id: toolDto.competition_id,
    tool_name: toolDto.tool_name,
    points: toolDto.tool_points,
    price: toolDto.tool_price,
    tool_image_path:toolDto.tool_image
  };

  if (!competitionTool.competition_id || !competitionTool.tool_image_path) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  let isExist = await competitionToolsDao.isExist(toolId);
  if (!isExist) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  if(toolDto.tool_image_path){

      let result = await s3Uploader.uploadAvatar(competitionTool.tool_image_path);
      competitionTool.s3_id = result.s3_id;
      delete competitionTool.tool_image_path
  }

  const result = await competitionToolsDao.updateCompetitionTool(toolId , competitionTool)
  return result

}

exports.getAllTools  = ( ) => {

  return competitionToolsDao.getAllTools()
}

exports.getToolWithId = ( tool_id ) => {

 return  competitionToolsDao.getToolWithId(tool_id)

}