
const express = require('express')
const router  = express.Router()
const authController = require('../../auth/web_app/auth_controller');
const competitionToolsController = require('./competition_tools_controller')



router.route('/').
get( authController.authenticate , 
    competitionToolsController.getAlTools)

module.exports = router