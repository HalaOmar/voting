
const serviceTools = require('../competition_tools_service')
const resWrapper   = require('../../commons/http_res_wrapper')

exports.getAlTools = async ( req  , res ) => {

    try{

        const tools = await serviceTools.getAllCompetitionTools()
        resWrapper.success(tools)

    }catch(e){
        resWrapper.error(e)

    }
}