const models = require('../models');
const Cities = models.cities;

exports.getAllCities = async (stateId) => {
  let results = await Cities.findAll({
    where: {
      state_id: stateId
    }
  });
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
};
