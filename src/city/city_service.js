const cityDao = require('./city_dao');

exports.getAllCities = (state_id) => {
  return cityDao.getAllCities(state_id);
};