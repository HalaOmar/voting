const express = require('express');
const cityController = require('./city_controller');

const router = express.Router();

router.route('/')
  .get(cityController.getAllCities);


module.exports = router;
