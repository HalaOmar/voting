const cityService = require('../city_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.getAllCities = async (req, res) => {
  try {
    const stateId = req.query.state_id;
    let results = await cityService.getAllCities(stateId);
    resWrapper.success(res, results);
  } catch (error) {
    resWrapper.error(res, error);
  }
};