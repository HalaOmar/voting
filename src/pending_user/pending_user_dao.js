const models = require('../models/index');
const PendingUser = models.pending_users;

exports.getAll = function () {
  return PendingUser.findAll()
};

exports.getByMobile = function (mobile) {
  return PendingUser.findOne({
    where: {
      mobile: mobile
    }
  })
};

exports.addOrUpdate = function (mobile) {
  let dto = {
    mobile: mobile,
  };

  return PendingUser.upsert(dto)
};

exports.delete = function (mobile) {
  return PendingUser.destroy({where: {mobile: mobile}})
};
