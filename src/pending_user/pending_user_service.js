const dao = require('./pending_user_dao');

exports.addPendingUser = async (mobile) => {
  return dao.addOrUpdate(mobile);
};

exports.getByMobile = async (mobile) => {
  return dao.getByMobile(mobile);
};

exports.deletePendingUser = async (mobile) => {
  return dao.delete(mobile);
};
