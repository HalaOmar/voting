const i18n = require('i18n');
const deviceTokensService = require('./device_token_service');
const fcm = require('../libs/fcm');
const resWrapper = require('../commons/http_res_wrapper');

exports.createDeviceToken = async function (req, response) {
  const userId = req.user.user.user_id;
  const device_token = req.body.device_token;
  const tokenType = req.body.token_type;

  const deviceToken = {
    user_id: userId,
    provider_id: null,
    device_token: device_token,
    token_type: tokenType
  };

  try {
    let result = await deviceTokensService.createDeviceToken(deviceToken);
    response.status(200).send(result);
  } catch (error) {
    console.log(error);
    resWrapper.handle(500, error, response);
  }
};

exports.broadcast = async function (req, res) {
  const topic = req.body.topic;
  const title = req.body.title;
  const body = req.body.body;

  try {
    await fcm.sendToTopic(topic, body, title);
    res.status(200).send();
  } catch (e) {
    resWrapper.handle(500, e, res);
  }
};
