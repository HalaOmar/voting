const deviceTokenDao = require('./device_token_dao');

exports.createDeviceToken = async (deviceToken) => {
  await deviceTokenDao.deleteDeviceToken(deviceToken.device_token);
  return deviceTokenDao.addDeviceToken(deviceToken)
};

exports.getUsersDeviceTokens = (userIds) => {
  return deviceTokenDao.getUsersDeviceTokens(userIds);
};
