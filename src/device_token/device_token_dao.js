const models = require('../models/index');
const DeviceToken = models.device_tokens;

exports.addDeviceToken = async function (deviceToken) {
  let dto = {
    user_id: deviceToken.user_id,
    provider_id: deviceToken.provider_id,
    device_token: deviceToken.device_token,
    token_type: deviceToken.token_type
  };

  let code = await DeviceToken.create(dto);
  return Promise.resolve(code.toJSON())
};

exports.getUsersDeviceTokens = function (userIds) {
  return DeviceToken.findAll({
    where: {
      user_id: userIds
    }
  })
};

exports.deleteDeviceToken = function (device_token) {
  return DeviceToken.destroy({
    where: {
      device_token: device_token
    }
  })
};

exports.getByUserId = async (userId) => {
  let result = await DeviceToken.findOne({
    where: {
      user_id: userId
    },
  });

  if (result) {
    result = result.toJSON();
  }

  return Promise.resolve(result);
};
