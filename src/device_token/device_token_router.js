const express = require('express');
const authController = require('../auth/web_app/auth_controller')
const deviceTokenController = require('./device_token_controller')

const router = express.Router();

router.route('/')
  .post(authController.authenticate,
    deviceTokenController.createDeviceToken);

module.exports = router;
