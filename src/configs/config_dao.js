const models = require('../models/index');
const Config = models.configs;

exports.add = async function (key, value) {
  let dto = {
    key: key,
    value: value
  };

  let code = await Config.create(dto);
  return Promise.resolve(code.toJSON());
};

exports.update = function (key, value) {
  return Config.update({
    value: value
  }, {
    where: {
      key: key
    }
  });
};

exports.getByKey = async (key) => {
  let config = await Config.findOne({
    where: {
      key: key,
    }
  });

  return Promise.resolve(config ? config.toJSON() : null);
};


exports.getAll = async function () {
  let configs = await Config.findAll();
  configs = configs.map(config => config.toJSON());
  return Promise.resolve(configs)
};
