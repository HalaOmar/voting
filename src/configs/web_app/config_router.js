const express = require('express');
const authController = require('../../auth/web_app/auth_controller');
const configController = require('./config_controller');

const router = express.Router();

router.route('/')
  .get(authController.authenticate,
    configController.getConfigs)

router.route('/:key')
  .get(authController.authenticate,
    configController.getByKey)

module.exports = router;
