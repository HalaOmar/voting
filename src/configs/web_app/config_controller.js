const i18n = require('i18n');
const configService = require('../config_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.getConfigs = async (req, res) => {

  try {
    let results = await configService.getConfigs();
    resWrapper.success(res, results);
  } catch (error) {
    resWrapper.error(res, error);
  }
};

exports.getByKey = async (req, res) => {

  let key = req.params.key;

  try {
    let result = await configService.getByKey(key);
    resWrapper.success(res, result);
  } catch (error) {
    resWrapper.error(res, error);
  }
};
