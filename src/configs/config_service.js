const configDao = require('./config_dao');
const Errors = require('../commons/errors/exceptions');

exports.getConfigs = () => {
  return configDao.getAll();
};

exports.getByKey = async (key) => {
  let config = await configDao.getByKey(key);
  if (!config) {
    throw new Errors.NotFoundException();
  }
  return await config;
};
