const i18n = require('i18n');
const competitionService = require('../competition_service')
const resWrapper = require('../../commons/http_res_wrapper');

exports.addCompetition = async (req, res) => {
  let dto = {
    started_at: (req.body.started_at) ? req.body.started_at : null,
    ended_at: (req.body.ended_at) ? req.body.ended_at : null,
    active: (req.body.active === null || req.body.active === undefined) ? null : req.body.active,
  }
  try {
    let createdCompetition = await competitionService.addCompetition(dto);
    resWrapper.success(res, createdCompetition);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.updateCompetition = async (req, res) => {
  let competitionId = req.params.competition_id;
  let dto = {
    started_at: (req.body.started_at) ? req.body.started_at : null,
    ended_at: (req.body.ended_at) ? req.body.ended_at : null,
    active: (req.body.active === null || req.body.active === undefined) ? null : req.body.active,
  }

  try {
    let result = await competitionService.updateCompetition(competitionId, dto);
    resWrapper.success(res, result);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getCompetitionById = async (req, res) => {
  let competitionId = req.params.competition_id;
  try {
    let result = await competitionService.getCompetitionById(competitionId);
    resWrapper.success(res, result);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.deleteCompetition = async (req, res) => {
  let competitionId = req.params.competition_id;
  try {
    await competitionService.deleteCompetition(competitionId);
    resWrapper.success(res);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getAllCompetitions = async (req, res) => {
  try {
    let results = await competitionService.getAllCompetitions();
    resWrapper.success(res, results);
  } catch (e) {
    resWrapper.error(res, e);
  }
};