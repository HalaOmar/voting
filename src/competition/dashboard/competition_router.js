const express = require('express')
const competitionRouter = express.Router()
const authController = require('../../auth/web_app/auth_controller');
const competitionController = require('./competition_controller')

competitionRouter.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionController.getAllCompetitions)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionController.addCompetition);

competitionRouter.route('/:competition_id')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionController.getCompetitionById)
  .put(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionController.updateCompetition)
  .delete(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionController.deleteCompetition)

module.exports = competitionRouter