const i18n = require('i18n');
const competitionDao = require('./competition_doa')
const idGenerator = require('../commons/id_generator');
const Errors = require('../commons/errors/exceptions');
const MessageKeys = require('../commons/constants').MessageKeys;

exports.addCompetition = async (competitionDto) => {
  let competition = {
    started_at: competitionDto.started_at,
    ended_at: competitionDto.ended_at,
    active: competitionDto.active,
  };

  if (!competitionDto.started_at || !competition.ended_at) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  competition.competition_id = idGenerator.generateId();

  await competitionDao.addCompetition(competition);
  return competitionDao.getCompetitionById(competition.competition_id);
}

exports.deleteCompetition = async (competitionId) => {
  if (!competitionId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await competitionDao.deleteCompetitionById(competitionId);
  return Promise.resolve();
}

exports.updateCompetition = async (competitionId, competitionDto) => {
  let competition = {
    started_at: competitionDto.started_at,
    ended_at: competitionDto.ended_at,
    active: competitionDto.active,
  };

  if (!competitionDto.started_at || !competition.ended_at) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await competitionDao.updateCompetition(competitionId, competitionDto);
  return competitionDao.getCompetitionById(competitionId);
}

exports.getAllCompetitions = async () => {
  let competitions = await competitionDao.getAllCompetitions();
  return Promise.resolve(competitions)
}

exports.getCompetitionById = async (competitionId) => {
  let competition = await competitionDao.getCompetitionById(competitionId);
  if (competition === null)
    throw new Errors.InvalidInputException();

  return Promise.resolve(competition);
}