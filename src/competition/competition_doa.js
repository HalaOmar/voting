const models = require('../models/index')
const Competition = models.competitions;

exports.addCompetition = async (competition) => {
  let result = await Competition.create(competition);
  return Promise.resolve(result.toJSON());
}

exports.getAllCompetitions = async () => {
  let results = await Competition.findAll();
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
}

exports.getCompetitionById = async (competitionId) => {
  let result = await Competition.findOne({
    where: {
      competition_id: competitionId
    },
  });

  if (result) {
    result = result.toJSON();
  }

  return Promise.resolve(result);
}

exports.deleteCompetitionById = async (competitionId) => {
  await Competition.destroy({
    where: {
      competition_id: competitionId
    }
  });
  return Promise.resolve();
}

exports.updateCompetition = (competitionId, competition) => {
  return Competition.update(competition, {
    where: {
      competition_id: competitionId
    }
  });
}