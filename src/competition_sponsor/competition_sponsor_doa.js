const models = require('../models/index')
const CompetitionSponsor = models.competition_sponsors;

const includeSponsor = {model: models.sponsors, as: 'sponsor'};

const fullIncludes = [
  includeSponsor,
];

exports.addCompetitionSponsor = async (competitionSponsor) => {
  let dto = {
    competition_id: competitionSponsor.competition_id,
    sponsor_id: competitionSponsor.sponsor_id,
  }
  let result = await CompetitionSponsor.create(dto);
  return Promise.resolve(result.toJSON());
};

exports.getByCompetitionIdSponsorId = async (competitionId, sponsorId) => {
  let result = await CompetitionSponsor.findOne({
    where: {
      competition_id: competitionId,
      sponsor_id: sponsorId,
    },
    include: fullIncludes,
  });

  if (result) {
    result = result.toJSON();
  }

  return Promise.resolve(result);
}

exports.isExist = async (competitionId, sponsorId) => {
  let result = await CompetitionSponsor.findOne({
    where: {
      competition_id: competitionId,
      sponsor_id: sponsorId,
    },
  });

  return Promise.resolve(result !== null);
}

exports.getAllCompetitionSponsors = async (competitionId) => {
  let results = await CompetitionSponsor.findAll({
    where: {
      competition_id: competitionId,
    },
    include: fullIncludes,
  });
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
};

exports.deleteCompetitionSponsor = async (competitionId, sponsorId) => {
  await CompetitionSponsor.destroy({
    where: {
      competition_id: competitionId,
      sponsor_id: sponsorId,
    }
  });
  return Promise.resolve();
};

exports.getAllSponsorsOnThisCompetition = async ( competitionId , startIndex , lim ) =>{

  const { count, rows } = await CompetitionSponsor.findAndCountAll({
    where: {
      competition_id : competitionId

    },
      offset: startIndex,
      limit: lim ,
      include: fullIncludes,
    });
    rows = rows.map(row => row.toJSON());
    
    return Promise.resolve({
        sponsors_count : count ,
        sponsors : rows
    })
}