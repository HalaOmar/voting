const i18n = require('i18n');
const competitionSponsorDao = require('./competition_sponsor_doa')
const Errors = require('../commons/errors/exceptions');
const MessageKeys = require('../commons/constants').MessageKeys;

exports.addCompetitionSponsor = async (competitionDto) => {
  let competitionSponsor = {
    competition_id: competitionDto.competition_id,
    sponsor_id: competitionDto.sponsor_id,
  };

  if (!competitionSponsor.competition_id || !competitionSponsor.sponsor_id) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  let isExist = await competitionSponsorDao.isExist(competitionSponsor.competition_id, competitionSponsor.sponsor_id);
  if (isExist) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await competitionSponsorDao.addCompetitionSponsor(competitionSponsor);

  return competitionSponsorDao.getByCompetitionIdSponsorId(competitionSponsor.competition_id, competitionSponsor.sponsor_id);
}

exports.deleteCompetitionSponsor = async (competitionId, sponsorId) => {
  if (!competitionId || !sponsorId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await competitionSponsorDao.deleteCompetitionSponsor(competitionId, sponsorId);
  return Promise.resolve();
}

exports.getAllCompetitionSponsors = async (competitionId) => {
  if (!competitionId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }
  let competitions = await competitionSponsorDao.getAllCompetitionSponsors(competitionId);
  return Promise.resolve(competitions)
}