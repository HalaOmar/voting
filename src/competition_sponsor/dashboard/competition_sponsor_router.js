const express = require('express')
const router = express.Router()
const authController = require('../../auth/web_app/auth_controller');
const competitionSponsorController = require('./competition_sponsor_controller')

router.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionSponsorController.getAllCompetitionSponsors)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionSponsorController.addCompetitionSponsor)
  .delete(authController.authenticate,
    authController.isAdminOrSupervisor,
    competitionSponsorController.deleteCompetitionSponsor)


module.exports = router