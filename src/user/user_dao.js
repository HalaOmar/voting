const models = require('../models/index');
const User = models.users;
const Op = models.Sequelize.Op;
const moment = require('moment');

const includeRole = {model: models.roles, as: 'role'};

const fullIncludes = [
  includeRole
];

exports.getAllUsers = async (pageSize = 10, pageNumber = 0, mobile, gender, rangeFrom, rangeTo,
                             searchKeyWord, withTotalCount = false) => {
  let offset = pageNumber * pageSize;
  let order = [["created_at", "DESC"]];
  let options = {
    order: order,
    offset: offset,
    limit: pageSize,
    raw: false,
    include: fullIncludes,
  };

  options.where = {};

  if (searchKeyWord) {
    let search = {
      [Op.or]: [
        {full_name: {[Op.substring]: searchKeyWord}},
        {mobile: {[Op.substring]: searchKeyWord}}
      ]
    };
    options.where = search;
  }

  if (mobile) {
    options.where.mobile = {
      [Op.substring]: mobile
    }
  }

  if (gender) {
    options.where.gender = gender
  }

  if (rangeFrom && rangeTo) {
    let date_from = moment().subtract(rangeFrom, 'year').endOf('year');
    let date_to = moment().subtract(rangeTo, 'year').startOf('year');
    options.where.date_of_birth = {[Op.between]: [date_to, date_from]}
  }

  if (!withTotalCount)
    return _getUsers(options);
  else
    return _getUsersWithCount(options, pageNumber, pageSize);
};

exports.getUsersCount = async (gender = null) => {
  let where = {}
  if (gender) {
    where.gender = gender
  }
  let totalUsers = await User.count({where});
  return {total: totalUsers};
};

exports.getAll = async () => {
  let users = await User.findAll();
  users = users.map(user => user.toJSON());
  return Promise.resolve(users);
};

exports.getById = async (userId) => {
  let user = await User.findOne({
    where: {
      user_id: userId
    },
    include: fullIncludes
  });

  if (user) {
    user = user.toJSON();
  }

  return Promise.resolve(user);
};

exports.getByMobile = async (mobile) => {
  let user = await User.findOne({
    where: {
      mobile: mobile
    },
    include: fullIncludes,
  });

  if (user) {
    user = user.toJSON()
  }

  return Promise.resolve(user);
};

exports.createNewUser = async (user) => {
  let dto = {
    user_id: user.user_id,
    full_name: user.full_name,
    date_of_birth: user.date_of_birth,
    email: user.email,
    mobile: user.mobile,
    blocked: user.blocked,
    gender: user.gender,
    role_id: user.role_id,
    shop_id: user.shop_id,
  };

  let result = await User.create(dto);
  return Promise.resolve(result.toJSON())
};

exports.addNewUser = async (user) => {
  let dto = {
    user_id: user.user_id,
    mobile: user.mobile,
    full_name: user.full_name,
    gender: user.gender,
    date_of_birth: user.date_of_birth,
    role_id: user.role_id,
  };

  let result = await User.create(dto);
  return Promise.resolve(result.toJSON())
};

exports.updateMyUserProfile = (userId, user) => {
  return User.update(user, {
    where: {
      user_id: userId
    },
    fields: ['full_name', 'email', 'avatar_image', 'gender', 'date_of_birth']
  })
};

exports.updateOtherUser = (userId, user) => {
  return User.update(user, {
    where: {
      user_id: userId
    },
    fields: ['full_name', 'email', 'avatar_image', 'role_id', 'blocked', 'gender', 'date_of_birth']
  })
};

exports.updateUserAvatar = (userId, s3Id) => {
  return User.update({s3_id: s3Id}, {
    where: {
      user_id: userId
    }
  })
};

exports.deleteById = (userId) => {
  return User.destroy({where: {user_id: userId}})
};

exports.getByRoleId = async (roleId) => {
  let users = await User.findAll({
    where: {
      role_id: roleId
    }
  });

  users = users.map(user => user.toJSON());
  return Promise.resolve(users)
};


exports.mapUsers = (result) => {
  return result.map(user => {
    return this.mapUser(user);
  });
};

exports.mapUser = (user) => {
  let res = user.toJSON();

  if (res.role)
    res.role = res.role.toJSON();

  return res;
};

_getUsers = async (options) => {
  let users = await User.findAll(options);
  users = this.mapUsers(users);
  return Promise.resolve(users);
};

_getUsersWithCount = async (options, pageNumber, pageSize) => {
  let results = await User.findAndCountAll(options);
  results.rows = this.mapUsers(results.rows);

  let response = {
    results: results.rows,
    page_number: pageNumber,
    page_size: pageSize,
    results_count: results.rows.length,
    total_count: results.count,
  };

  return Promise.resolve(response);
};