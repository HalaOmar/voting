const i18n = require('i18n');
const Errors = require('../commons/errors/exceptions');

const userDao = require('./user_dao');
const mobileValidator = require('./mobile_validator');
const mobileCodeService = require('../mobile_code/mobile_code_service');
const pendingUserService = require('../pending_user/pending_user_service');
const accessTokenService = require('../access_token/access_token_service');
const sms = require('../libs/sms');
const Roles = require('../commons/constants').Roles;
const OtpTypes = require('../commons/constants').OtpTypes;
const idGenerator = require('../commons/id_generator');
const s3Uploader = require('../commons/s3Uploader');
const mobileNumberFix = require('../commons/mobile_number_fix');
const moment = require('moment');
const logger = require('../libs/logger');
const {MessageKeys} = require('../commons/constants');

exports.addUser = async (mobile) => {
  if (mobile === undefined || mobile === null) {
    throw new Errors.InvalidInputException();
  }

  // Fix & Validate mobile number
  mobile = mobileNumberFix.fixMobileNumber(mobile);
  let errors = mobileValidator.isValidMobile(mobile);
  if (errors.length > 0) {
    throw new Errors.InvalidMobileNumberException();
  }

  let user = await userDao.getByMobile(mobile);
  if (!user) {
    await pendingUserService.addPendingUser(mobile);
  }

  let mobileCode = await mobileCodeService.createMobileCode(mobile, OtpTypes.USER_CODE);
  const message = i18n.__(MessageKeys.YOUR_ACTIVATION_CODE_IS, {code: mobileCode.code});

  if (process.env.NODE_ENV !== 'local' && process.env.NODE_ENV !== 'development') {
    await sms.send(mobile, message)
  }

  return Promise.resolve({
    mobile: mobile,
    success: true
  })
};

exports.verifyOTP = async (mobile, code) => {
  if (mobile === undefined || mobile === null || code === undefined || code === null) {
    throw new Errors.InvalidInputException();
  }

  // Fix & Validate mobile number
  mobile = mobileNumberFix.fixMobileNumber(mobile);
  let errors = mobileValidator.isValidMobile(mobile);
  if (errors.length > 0) {
    throw new Errors.InvalidMobileNumberException();
  }

  // Fix & Validate code arabic digits
  code = code.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
    return d.charCodeAt(0) - 1632;
  });

  const codeType = OtpTypes.USER_CODE;

  let mobileCode = await mobileCodeService.getByMobileCode(mobile, code, codeType);
  if (!mobileCode) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_OTP));
  }

  let user = await userDao.getByMobile(mobile);

  // Create web_app , if it's a first time login
  if (!user) {
    let newUser = {
      user_id: idGenerator.generateId(),
      mobile: mobile,
      role_id: Roles.USER
    };

    await userDao.addNewUser(newUser);
    user = await userDao.getByMobile(mobile);
  }

  await pendingUserService.deletePendingUser(mobile);
  await mobileCodeService.setUsed(mobile, code);
  let accessToken = await accessTokenService.createTokenForUser(user.user_id);

  return Promise.resolve({
    user: user,
    access_token: accessToken.access_token
  })
};

exports.getMyProfile = async (userId) => {
  if (userId === undefined || userId === null) {
    throw new Errors.InvalidInputException();
  }

  let user = await userDao.getById(userId);
  if (!user) {
    throw new Errors.UserNotFoundException();
  }

  return Promise.resolve(user);
};

exports.getUserDetails = async (userId) => {
  if (userId === undefined || userId === null) {
    throw new Errors.InvalidInputException();
  }

  let user = await userDao.getById(userId);
  if (!user) {
    throw new Errors.UserNotFoundException();
  }

  let result = {
    user_id: user.user_id,
    full_name: user.full_name,
    image_url: user.image_url,
  };

  return Promise.resolve(result);
};


exports.getById = async (userId) => {
  if (userId === undefined || userId === null) {
    throw new Errors.InvalidInputException();
  }

  let user = await userDao.getById(userId);
  if (!user) {
    throw new Errors.UserNotFoundException();
  }

  return Promise.resolve(user);
};

exports.updateMyUserProfile = async (userId, user) => {

  if (!user.full_name) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.ENTER_VALID_NAME));
  }

  let reg = /^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF A-Za-z]+$/;

  if (user.full_name.length < 2 || !reg.test(user.full_name)) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.ENTER_VALID_NAME));
  }

  if (user.date_of_birth && user.date_of_birth !== '') {
    logger.info(`date_of_birth value : ${user.date_of_birth}`);
    let date_of_birth = moment(user.date_of_birth, 'YYYY-MM-DD', 'UTC');

    if (date_of_birth.format('YYYY-MM-DD') === 'Invalid date') {
      let split = user.date_of_birth.split('-');
      let yyyy = split[0].replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
        return d.charCodeAt(0) - 1632;
      });
      let mm = split[1].replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
        return d.charCodeAt(0) - 1632;
      });
      let dd = split[2].replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
        return d.charCodeAt(0) - 1632;
      });
      user.date_of_birth = `${yyyy}-${mm}-${dd}`;
    }

  }

  try {
    await userDao.updateMyUserProfile(userId, user);
  } catch (error) {
    logger.error(error);
    throw new Errors.InvalidInputException();
  }

  if (user.file) {
    let result = await s3Uploader.uploadAvatar(user.file);
    await userDao.updateUserAvatar(userId, result.s3_id);
  }

  let updatedUser = await userDao.getById(userId);
  return Promise.resolve(updatedUser);
};

exports.updateOtherUser = async function (userId, user) {
  await userDao.updateOtherUser(userId, user);

  if (user.file) {
    let result = await s3Uploader.uploadAvatar(user.file);
    await userDao.updateUserAvatar(userId, result.s3_id);
  }

  let updatedUser = await userDao.getById(userId);
  return Promise.resolve(updatedUser)
};

exports.createNewUser = async (user) => {
  if (user.mobile === null || user.mobile === undefined) {
    throw new Errors.InvalidMobileNumberException();
  }

  user['user_id'] = idGenerator.generateId();

  // Fix & Validate mobile number
  user.mobile = mobileNumberFix.fixMobileNumber(user.mobile);
  let errors = mobileValidator.isValidMobile(user.mobile);
  if (errors.length > 0) {
    throw new Errors.InvalidMobileNumberException();
  }

  let userExist = await userDao.getByMobile(user.mobile);
  if (userExist) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.USER_ALREADY_EXIST));
  }

  let createdUser = await userDao.createNewUser(user);

  if (user.file) {
    let result = await s3Uploader.uploadAvatar(user.file);
    await userDao.updateUserAvatar(createdUser.user_id, result.s3_id);
  }

  if (user.role_id === Roles.CELEBRITY) {
    let shopUser = await shopUserDao.getByUserIdShopIdRoleId(user.user_id, user.shop_id, user.role_id);
    if (shopUser === null) {
      await shopUserDao.addNewUser({
        user_id: user.user_id,
        shop_id: user.shop_id,
        role_id: user.role_id,
      });
    }
  }

  let updatedUser = await userDao.getById(createdUser.user_id);
  return Promise.resolve(updatedUser);
};

exports.findUser = async (userId) => {
  return userDao.getById(userId);
};

exports.getByRoleId = async (roleId) => {
  return userDao.getByRoleId(roleId);
};

exports.getAllUsers = async (pageSize = 20, pageNumber = 0, mobile, gender, rangeFrom, rangeTo,
                             searchKeyWord, withTotalCount) => {
  return userDao.getAllUsers(pageSize, pageNumber, mobile, gender, rangeFrom, rangeTo, searchKeyWord, withTotalCount);
};

exports.getUsersCount = async () => {
  return userDao.getUsersCount();
};