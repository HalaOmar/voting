const validator = require('validator');
const logger = require('../libs/logger');
const i18n = require('i18n');
const { MessageKeys } = require('../commons/constants');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const fileName = __filename.slice(__dirname.length + 1);

exports.isValidMobile = (mobile) => {
  logger.debug(`${fileName}::isValidMobile - ${mobile}`);

  let error_messages = [];

  if (mobile === null || mobile === '') {
    error_messages.push(i18n.__(MessageKeys.MOBILE_IS_REQUIRED))
  } else {

    try {
      let parsedMobile = phoneUtil.parse(mobile);
      if (!phoneUtil.isValidNumber(parsedMobile)) {
        error_messages.push(i18n.__(MessageKeys.INVALID_MOBILE))
      }
    } catch (e) {
      logger.error(`isValidMobile Parse Error: ${e}`);
      error_messages.push(i18n.__(MessageKeys.INVALID_MOBILE))
    }
  }

  logger.debug(`${fileName}::isValidMobile - Errors: ${error_messages.length}`);

  return error_messages
};
