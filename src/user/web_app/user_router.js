const express = require('express');
const router = express.Router();
const authController = require('../../auth/web_app/auth_controller');
const userController = require('./user_controller');
const uploadController = require('../../commons/upload_middleware');

router.route('/me')
  .get(authController.authenticate,
    userController.getMyProfile);

router.route('/:user_id')
  .get(authController.authenticate,
    userController.getUserDetails)
  .put(authController.authenticate,
    authController.authorize,
    uploadController.uploadFiles,
    userController.updateMyUserProfile);

module.exports = router;
