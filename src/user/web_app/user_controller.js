const i18n = require('i18n');
const userService = require('../user_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.addUser = async (req, res) => {
  try {
    let mobile = req.body.mobile;
    let user = await userService.addUser(mobile);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.verifyOTP = async (req, res) => {
  try {
    let mobile = req.body.mobile;
    let code = req.body.code;
    let user = await userService.verifyOTP(mobile, code);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getMyProfile = async (req, res) => {
  let userId = req.user.user.user_id;
  try {
    let user = await userService.getMyProfile(userId);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getUserDetails = async (req, res) => {
  let userId = req.params.user_id;
  try {
    let user = await userService.getUserDetails(userId);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.updateMyUserProfile = async (req, res) => {
  let user = req.body;
  let userId = req.user.user.user_id;
  if (req.files.length > 0) {
    user.avatar_image = req.files[0].filename;
    user.file = req.files[0].path
  }

  try {
    let updatedUser = await userService.updateMyUserProfile(userId, user);
    resWrapper.success(res, updatedUser);
  } catch (e) {
    resWrapper.error(res, e);
  }
};