const i18n = require('i18n');
const Roles = require('../../commons/constants').Roles;
const userService = require('../user_service');
const resWrapper = require('../../commons/http_res_wrapper');

exports.addUser = async (req, res) => {
  try {
    let user = await userService.addUser(req.body.mobile);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.verifyOTP = async (req, res) => {
  try {
    let mobile = req.body.mobile;
    let code = req.body.code;
    let user = await userService.verifyOTP(mobile, code);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getById = async (req, res) => {
  let userId = req.params.user_id;
  try {
    let user = await userService.getById(userId);
    resWrapper.success(res, user);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.updateMyUserProfile = async (req, res) => {
  let user = req.body;
  let userId = req.user.user.user_id;
  if (req.files.length > 0) {
    user.avatar_image = req.files[0].filename;
    user.file = req.files[0].path
  }

  try {
    let updatedUser = await userService.updateMyUserProfile(userId, user);
    resWrapper.success(res, updatedUser);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getAllSupervisors = async function (req, res) {
  try {
    let users = await userService.getByRoleId(Roles.SUPERVISOR);
    resWrapper.success(res, users);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.list = async function (req, res) {
  let pageNumber = req.query.page_number ? parseInt(req.query.page_number) : 0;
  let pageSize = (req.query.page_size && req.query.page_size < 50) ? parseInt(req.query.page_size) : 20;
  let mobile = req.query.mobile ? req.query.mobile : null;
  let gender = req.query.gender ? req.query.gender : null;
  let rangeFrom = req.query.range_from ? req.query.range_from : null;
  let rangeTo = req.query.range_to ? req.query.range_to : null;
  let searchKeyWord = req.query.search;
  let withTotalCount = true;

  try {
    let users = await userService.getAllUsers(pageSize, pageNumber, mobile, gender,
      rangeFrom, rangeTo,  searchKeyWord, withTotalCount);
    resWrapper.success(res, users);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.getUsersCount = async function (req, res) {

  try {
    let users = await userService.getUsersCount();
    resWrapper.success(res, users);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.updateOtherUser = async (req, res) => {
  let user = req.body;
  let userId = req.params.user_id;

  if (req.files.length > 0) {
    user.avatar_image = req.files[0].filename;
    user.file = req.files[0].path
  }

  try {
    let updatedUser = await userService.updateOtherUser(userId, user);
    resWrapper.success(res, updatedUser);
  } catch (e) {
    resWrapper.error(res, e);
  }
};

exports.createNewUser = async function (req, res) {
  let user = {
    full_name: req.body.full_name,
    mobile: req.body.mobile,
    password: req.body.password,
    role_id: req.body.role_id,
    shop_id: req.body.shop_id,
  };

  try {
    let createdUser = await userService.createNewUser(user);
    resWrapper.success(res, createdUser);
  } catch (e) {
    resWrapper.error(res, e);
  }
};


