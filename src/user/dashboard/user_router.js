const express = require('express');
const router = express.Router();
const authController = require('../../auth/web_app/auth_controller');
const userController = require('./user_controller');
const uploadController = require('../../commons/upload_middleware');

router.route('/')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    userController.list)
  .post(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    userController.createNewUser);

router.route('/total')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    userController.getUsersCount);

router.route('/:user_id')
  .get(authController.authenticate,
    authController.isAdminOrSupervisor,
    userController.getById)
  .put(authController.authenticate,
    authController.isAdminOrSupervisor,
    uploadController.uploadFiles,
    userController.updateOtherUser);

module.exports = router;
