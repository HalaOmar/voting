const userPointsService = require ('../user_points_service')
// const userPurchases     = require ('../../users_purchases')
const IdGenerator      = require ('../../commons/id_generator')
const resWraper         = require ('../../commons/http_res_wrapper')

exports.addPointsToUser = async ( req , res  ) => {

    try {
    let voteObject = {

    purchaseId : IdGenerator.generateId() ,
    userId : req.user ? req.user.userId : null ,
    points : req.user ? req.user.points : 0
    } 

    const result = await userPointsService.addPointsToUser(purchaseObject)
    resWraper.success( res , result )

    } catch (e) {

    resWraper.error( res , e )

}

}

exports.getAllMyPoints = async ( req , res ) => {

    try{

        let userId = req.body.userId ? req.body.userId : null

        let result = await userPointsService.getAllMyPoints(userId)

        resWraper.success(res , result)

    }catch(e){
        resWraper.error(res , e)

    }
    
}