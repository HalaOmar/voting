const express = require('express')
const router  = express.Router()
const autController = require('../../auth/web_app/auth_controller')
const userPointsController = require('./user_points_controller')

router.route('/').
post( autController.authenticate , 
      userPointsController.getAllMyPoints)

router.route('/points').
post ( autController.authenticate , 
      userPointsController.addPointsToUser)


module.exports =router