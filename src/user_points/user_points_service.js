const i18n = require('i18n');
const userPointsDao = require('./user_points_doa')
const Errors = require('../commons/errors/exceptions');
const MessageKeys = require('../commons/constants').MessageKeys;
const IdGenerator      = require ('../commons/id_generator')

exports.addPointsToUser = async (purchaseObject) => {

  let userPurchase = {
    user_id    : purchaseObject.userId,
    purchaseId : purchaseObject.purchaseId , 
    points     : purchaseObject.points
  };

  if (!purchaseObject.userId || !purchaseObject.purchaseId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  await userPointsDao.addPointsToUser(userPurchase)

  return userPointsDao.getAllUserPoints(purchaseObject.userId);
}

exports.getAllMyPoints = async ( userId) => {
  
  if (!userId) {
    throw new Errors.InvalidInputException(i18n.__(MessageKeys.INVALID_INPUT));
  }

  const userbalance = await userPointsDao.getUserBalance(userId)

  if(!userbalance) {

   return Promise.resolve("Your Balance is Zero")

  } 

  return userPointsDao.getAllMyPoints(userId)
  
}

exports.userVoteToCelebrity = async ( vote ) => {
 console.log(` object :>> \n \n \n vote`);
 console.log({ ...vote});
  const user_balance = await userPointsDao.getUserBalance(vote.userId)

  if(!user_balance) {

   return Promise.resolve(0)

  } 
  let userPoints = {
    user_id    : vote.userId,
    points     : -1 ,
    purchase_id : IdGenerator.generateId()
  };
  return userPointsDao.addPointsToUser( userPoints)
  

}
