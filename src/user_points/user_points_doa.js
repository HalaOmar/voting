// const P = require('pino');
const { sequelize } = require('../models/index');
const models = require('../models/index')
const UserPonints = models.user_points;

const includeUser = {model: models.users, as: 'user'};
const includePurchase = {model: models.purchases, as: 'purchases'};

const fullIncludes = [
  includeUser, includePurchase
];

exports.addPointsToUser = async (purchaseObject) => {
  let dto = {
    user_id     : purchaseObject.user_id,
    purchase_id : purchaseObject.purchase_id,
    points      :purchaseObject.points
  }
  let result = await UserPonints.create(purchaseObject);
  return Promise.resolve(result.toJSON());
};



exports.isExist = async (userId) => {
  let result = await UserPonints.findOne({
    where: {
      user_id : userId
      
    },
  });

  return Promise.resolve(result !== null);
}

exports.getAllUsersAndTherePoints = async () => {
  let results = await UserPonints.findAll({

    include: fullIncludes,
  });
  results = results.map(result => result.toJSON());
  return Promise.resolve(results);
};

exports.getAllUserPoints = async ( userId , startIndex , lim ) =>{

  const { count, rows } = await UserPonints.findAndCountAll({
    where: {
      user_id : userId

    },
      offset: startIndex,
      limit: lim ,
      include: fullIncludes,
      raw :true
    });
    rows = rows.map(row => row.toJSON());
    
    return Promise.resolve({
        rows_count : count ,
        userPoints : rows
    })
}

exports.deleteAllUserPoints = async ( userId ) => {

  return  UserPonints.destroy( { } , {
                                  where : {
                                    user_id : userId
                                  }
                                })

}

exports.getUserBalance  = async ( userId ) => {
console.log(` uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuser :>> ${userId}`);
  let balance = await UserPonints.sum( 'points' , {
    where : {
      user_id : userId
    }
  })
  // console.log(` object :>> \n \n balance \n`);
        console.log( balance);
//       const balance = await  UserPonints.findAll( {
//           where:{
//             user_id : userId
//           } ,
//           attributes : [ sequelize.fn('sum',sequelize.col('points'))] ,
//           include : [includeUser],
//         })


      return Promise.resolve(balance)
    
}

exports.getAllMyPoints = async ( userId ) => {

  return UserPonints.findAll( {
    where:{
      user_id : userId
    } ,
    include : [ includeUser ] ,
    raw : true ,
    order: sequelize.literal('points DESC')
  })


}