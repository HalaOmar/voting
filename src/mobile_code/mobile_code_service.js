const logger = require('../libs/logger');
const util = require('util');
const dao = require('./mobile_code_dao');
const OtpTypes = require('../commons/constants').OtpTypes;
const moment = require('moment');

exports.createMobileCode = async function (mobile, codeType) {
  const mobileCode = {
    mobile: mobile,
    code_type: codeType,
    code: Math.floor(1000 + Math.random() * 9000),
    expire_at: moment().add(30, 'minutes')
  };

  if (process.env.NODE_ENV === 'local' || process.env.NODE_ENV === 'development') {
    mobileCode.code = "1234";
  }

  logger.debug(`Generate OTP - mobile: ${mobile}, otp: ${mobileCode.code}`);
  return dao.add(mobileCode)
};

exports.getByMobileCode = async function (mobile, code, codeType) {
  let mobileCode = {
    mobile: mobile,
    code: code,
    code_type: codeType,
    expire_at: Date.now(),
    used: false
  };
  return dao.getByMobileCode(mobileCode)
};

exports.setUsed = async function (mobile, code) {
  return dao.setUsed(mobile, code)
};
