const models = require('../models/index');
const Op = models.Sequelize.Op;
const MobileCode = models.mobile_codes;

exports.add = async function (mobileCode) {
  let dto = {
    mobile: mobileCode.mobile,
    code: mobileCode.code,
    code_type: mobileCode.code_type,
    expire_at: mobileCode.expire_at
  };

  let code = await MobileCode.create(dto);
  return Promise.resolve(code.toJSON());
};


exports.getByMobileCode = function (mobileCode) {
  return MobileCode.findOne({
    where: {
      mobile: mobileCode.mobile,
      code: mobileCode.code,
      code_type: mobileCode.code_type,
      expire_at: {
        [Op.gt]: mobileCode.expire_at
      },
      used: mobileCode.used
    }
  })
};

exports.setUsed = function (mobile, code) {
  return MobileCode.update({
    used: true
  }, {
    where: {
      mobile: mobile,
      code: code
    }
  })
};
