const resWrapper = require ('../../commons/http_res_wrapper')
const voteService = require('../vote_service')
exports.addVote = ( req , res ) =>{

    try{

    let userId = !req.user ? req.user.user_id : null
    let celebrityId = req.body.celebrityId ? req.body.celebrityId : null
    let points = req.body.points ? req.body.points : 0
    let result = voteService.addVote( userId , celebrityId , points )
    resWrapper.success(res , result  )
    
    }catch ( e ) {
    resWrapper.error(res , e )
    } 

}