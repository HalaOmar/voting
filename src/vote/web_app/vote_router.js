const express = require('express')
const vote_router = express.Router()
const auth_controller = require('../../auth/web_app/auth_controller')
const voteController = require('./vote_controller')


vote_router.route('/').
post ( auth_controller.authenticate ,
   vote )