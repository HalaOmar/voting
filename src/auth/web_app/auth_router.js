const express = require('express');
const authController = require('./auth_controller');
const userController = require('../../user/web_app/user_controller');
const limit = require('express-better-ratelimit');

const router = express.Router();

const mobileCodeLimiter = limit({
  duration: 1000 * 60, //1 minute
  max: 1000
});

router.route('/otp')
  .post(userController.verifyOTP);


router.route('/')
  .post(mobileCodeLimiter, userController.addUser)
  .delete(authController.authenticate, authController.logout);


module.exports = router;
