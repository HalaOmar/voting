const i18n = require('i18n');
const passport = require('passport');
const util = require('util');

const resWrapper = require('../../commons/http_res_wrapper');
const {Roles, MessageKeys} = require('../../commons/constants');
const TokenService = require('../../access_token/access_token_service');

exports.optionalAuthenticate = function (req, res, next) {
  passport.authenticate('bearer', {session: false}, function (err, user, info) {
    if (err || user === false) {
      next();
    } else {
      req.user = user;
      next()
    }
  })(req, res, next)
};

exports.authenticate = function (req, res, next) {
  
  passport.authenticate('bearer', {session: false}, function (err, user, info) {

    // authentication error
    if (err || user === false) {
      return res.status(401).send({
        code: 401,
        message: i18n.__(MessageKeys.INVALID_ACCESS_TOKEN)
      })
    }

    req.user = user;
    next()

  })(req, res, next)
};

exports.authorize = function (req, res, next) {
  let user_id = req.user.user.user_id;
  let requested_user_id = req.params.user_id;

  if (user_id === requested_user_id) {
    next();
  } else {
    res.status(401).send({
      code: 401,
      message: i18n.__(MessageKeys.INVALID_ACCESS_TOKEN)
    });
  }
};

exports.logout = async function (req, res) {
  try {
    await TokenService.deleteToken(req.user.token);
    res.status(200).send();
  } catch (e) {
    resWrapper.handle(500, e, res);
  }
};

exports.isAdminOrSupervisor = function (req, response, next) {
  let reqUser = req.user.user;
  let roleId = reqUser.role.role_id;

  if (isAdmin(roleId) || isSupervisor(roleId)) {
    next();
  } else {
    response.status(401).send({
      code: 401,
      message: i18n.__(MessageKeys.UNAUTHORIZED_ACCESS)
    });
  }
};

exports.isCelebrity = async (req, response, next) => {
  let reqUser = req.user.user;
  let roleId = reqUser.role.role_id;

  if (isCelebrity(roleId)) {
    next();
  } else {
    response.status(401).send({
      code: 401,
      message: i18n.__(MessageKeys.UNAUTHORIZED_ACCESS)
    });
  }

};

const isAdmin = function (roleId) {
  return roleId === Roles.ADMIN;
};

const isCelebrity = function (roleId) {
  return roleId === Roles.CELEBRITY;
};

const isSupervisor = function (roleId) {
  return roleId === Roles.SUPERVISOR;
};
