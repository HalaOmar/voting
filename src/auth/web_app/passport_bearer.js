const TokenService = require('../../access_token/access_token_service');
const UserService = require('../../user/user_service');
const util = require('util');
const BearerStrategy = require('passport-http-bearer').Strategy;

bearerAuth = async function (token, cb) {

  try {
    let result = await TokenService.findToken(token);
    if (!result) {
      return cb(true, null);
    }

    let user = await UserService.findUser(result.user_id);
    if (!user) {
      return cb(true, null);
    }
    return cb(null, {
        user: user,
        token: token
      });
  } catch (e) {
    return cb(true, null);
  }
};

exports.BearerStrategy = new BearerStrategy(bearerAuth);
