require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const i18n = require('i18n');
const passport = require('passport');
const helmet = require('helmet');
const chalk = require('chalk');
const bodyParser = require('body-parser');
const boolParser = require('express-query-boolean');
const http =require('http')
const socket     = require('socket.io')
const compression = require('compression');


const logger = require('../src/libs/logger');

const log = console.log;
const cyan = chalk.cyan;

const app = express();
const port = process.env.CLIENT_API_PORT;
/////////////////////////////////////////////////////////////////
const App = express()
const httpServer = require("http").createServer(App);
const options = { /* ... */ };
const io = require("socket.io")(httpServer, options);
io.on("connection", socket => { console.log("Hala u r connected");

 });
 
io.on("disconnect", socket => { 
  
console.log("Hala u r disconnected");
socket.on('disconnect') , ()=>{ socket.broadcast.emit("Hala Disconnected")} });

App.use( ( req , res , next ) => {
  res.locals.io = io
  next()})

App.get('/' , (req , res , next )=>{ res.sendFile(`${__dirname}/sockettest.html`)})
// httpServer.listen(3000 , ()=>{ console.log(`The server is running on port 3000`);});
App.use(bodyParser.json())
App.use('/api/v1/competition_voting', require('../src/competition_voting/Web-app/competition_voting_router'));


const pinoHttp = require('pino-http')({
  logger: logger,
  autoLogging: false,
});


app.use(helmet());
app.use(compression());
app.use(pinoHttp);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(boolParser());

i18n.configure({
  locales: ['ar', 'en'],
  directory: __dirname + '/../locales',
  defaultLocale: 'ar'
});

app.use(i18n.init);

app.use(passport.initialize());
passport.use(require('../src/auth/web_app/passport_bearer').BearerStrategy);

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.setHeader('Pragma', 'no-cache');
  res.setHeader('Expires', 0);
  next();
});

app.all('/*', function (req, res, next) {

  let accept = req.get("Accept-Language");
  accept = (accept === undefined) ? 'ar' : accept;

  if (accept) {
    i18n.setLocale(accept);
  }

  res.header("Content-Type", "application/json; charset=utf-8");
  next();

});

app.get('/', (req, res) => {
  res.send({
    message: 'Jilbab Voting API'
  })
});
 app.use( (req,res, next )=>{console.log(req.socket._peername);next()})
app.use('/api/v1/auth', require('../src/auth/web_app/auth_router'));
app.use('/api/v1/users', require('../src/user/web_app/user_router'));
app.use('/api/v1/device_tokens', require('../src/device_token/device_token_router'));
app.use('/api/v1/configs', require('../src/configs/web_app/config_router'));
app.use('/api/v1/cities', require('../src/city/web_app/city_router'));
app.use('/api/v1/states', require('../src/state/web_app/state_router'));
app.use('/api/v1/celebrities', require('../src/celebrity/web_app/celebrity_router'));
app.use('/api/v1/competition_tools', require('../src/competition_tools/web_app/competition_tools_router'));
app.use('/api/v1/user_points', require('../src/user_points/web_app/user_points_router'));
app.use('/api/v1/competition_voting', require('../src/competition_voting/Web-app/competition_voting_router'));
app.use('/api/v1/user_purchases_tools', require('../src/users_purchases_tools/web_app/user_purchases_tools_router'));



// Dashboard APIs
app.use('/api/v1/dashboard/users', require('../src/user/dashboard/user_router'));
app.use('/api/v1/dashboard/celebrities', require('../src/celebrity/dashboard/celebrity_router'));
app.use('/api/v1/dashboard/competitions', require('../src/competition/dashboard/competition_router'));
app.use('/api/v1/dashboard/competition_celebrities', require('../src/competition_celebrity/dashboard/competition_celebrity_router'));
app.use('/api/v1/dashboard/sponsors', require('../src/sponsor/dashboard/sponsor_router'));
app.use('/api/v1/dashboard/competition_sponsors', require('../src/competition_sponsor/dashboard/competition_sponsor_router'));
app.use('/api/v1/dashboard/competition_tools', require('../src/competition_tools/dashboard/competition_tools_router'));
app.use('/api/v1/dashboard/competition_voting', require('../src/competition_voting/dashboard/competition_voting_router'));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const error = new Error('Not Found');
  error.status = 404;
  next(error);
});

// error handler
app.use(function (error, req, res, next) {
  logger.error(`${error.status || 500} - ${error.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  error = process.env.NODE_ENV !== 'production' ? error : {status: error.status || 500, message: "Excution Error!"};
  res.status(error.status || 500);
  res.send({
    message: error.message
  });

});


app.listen(port, () => {
  log(cyan(`App listening on port: ${port} , environment: ${process.env.NODE_ENV}`))
});
