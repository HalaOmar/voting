MySQL:

### Create the database  (Buggy: didn't read charset/collate)
sequelize db:create --env local

### Drop the database
sequelize db:drop --env local

### Manual db creation
create database jilbab_voting_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

#### Create db user
CREATE USER 'user'@'localhost' IDENTIFIED BY '123';
GRANT ALL PRIVILEGES ON jilbab_voting_db.* TO 'user'@'localhost';
FLUSH PRIVILEGES;
exit;


### Run migrations
NODE_ENV=local sequelize db:migrate

### Run seeders
NODE_ENV=local sequelize db:seed:all
